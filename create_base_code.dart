String toCamelCase(String input) {
  final words = input.split('_');
  final camelCaseWords = words.map((word) => word.toLowerCase().capitalize());
  return camelCaseWords.join();
}

extension StringExtension on String {
  String capitalize() {
    return '${this[0].toUpperCase()}${substring(1)}';
  }
}

String generateBlocCode(String featureName) {
  final camelCaseName = toCamelCase(featureName);
  return '''
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import '${featureName}_event.dart';
import '${featureName}_state.dart';


class ${camelCaseName}Bloc extends Bloc<${camelCaseName}Event, ${camelCaseName}State> {
 
 
    void clearError() {
    add(ClearError());
  }

  @factoryMethod
  ${camelCaseName}Bloc() : super(${camelCaseName}State.initial()) {
    on<${camelCaseName}Event>((event, emit) async {
      /// *** Clear Error ***
      if (event is ClearError) {
        emit(
          state.rebuild(
            (b) => b..message = '',
          ),
        );
      }
    });
  }
}

  ''';
}

String generateEventCode(String featureName) {
  final camelCaseName = toCamelCase(featureName);
  return '''
import 'package:built_value/built_value.dart';

part '${featureName}_event.g.dart';

abstract class ${camelCaseName}Event {}

abstract class ClearError extends ${camelCaseName}Event
    implements Built<ClearError, ClearErrorBuilder> {
  ClearError._();

  factory ClearError([Function(ClearErrorBuilder b) updates]) = _\$ClearError;

  factory ClearError.initial() {
    return ClearError((b) => b);
  }
}

// TODO: Define events
  ''';
}

String generateStateCode(String featureName) {
  final camelCaseName = toCamelCase(featureName);
  return '''
import 'package:built_value/built_value.dart';
import '../../../../core/base_states/base_state.dart';

part '${featureName}_state.g.dart';

abstract class ${camelCaseName}State with BaseState implements Built<${camelCaseName}State, ${camelCaseName}StateBuilder> {
  ${camelCaseName}State._();

  factory ${camelCaseName}State([Function(${camelCaseName}StateBuilder b) updates]) = _\$${camelCaseName}State;
    factory ${camelCaseName}State.initial() {
    return ${camelCaseName}State(
      (b) => b
        ..isLoading = false
        ..error = false
        ..message = ''
    );
  }

}
  ''';
}

String generateRemoteClasses(String featureName) {
  final camelCaseName = toCamelCase(featureName);
  return ''' 
  import 'package:injectable/injectable.dart';
  import 'package:maids_test/core/data/base_remote_data_source.dart';

abstract class ${camelCaseName}RemoteDataSource extends BaseRemoteDataSource {
}

@LazySingleton(as: ${camelCaseName}RemoteDataSource)
class ${camelCaseName}RemoteDataImp extends BaseRemoteDataSourceImpl
    implements ${camelCaseName}RemoteDataSource {
  ${camelCaseName}RemoteDataImp({required super.dio, required super.localDataSource});
  }

  ''';
}

String generateLocalClasses(String featureName) {
  final camelCaseName = toCamelCase(featureName);
  return ''' 
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:maids_test/core/data/base_local_data_source.dart';

abstract class ${camelCaseName}LocalDataSource extends BaseLocalDataSource {}

@LazySingleton(as: ${camelCaseName}LocalDataSource)
class ${camelCaseName}LocalDataSourceImp extends BaseLocalDataSourceImp
    implements ${camelCaseName}LocalDataSource {
  ${camelCaseName}LocalDataSourceImp({required SharedPreferences sharedPreferences})
      : super(sharedPreferences: sharedPreferences);
}
  ''';
}

String generateRepository(String featureName) {
  final camelCaseName = toCamelCase(featureName);
  return '''
    abstract class ${camelCaseName}Repository {}
  ''';
}

String generateRepositoryImp(String featureName) {
  final camelCaseName = toCamelCase(featureName);
  return '''
import 'package:injectable/injectable.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:maids_test/core/network/network_info.dart';
import 'package:maids_test/core/data/base_repository.dart';
import 'dart:async';
import 'package:maids_test/features/$featureName/data/data_source/remote_data_source/${featureName}_remote_data_source.dart';
import 'package:maids_test/features/$featureName/domain/repository/${featureName}_repository.dart';
import 'package:maids_test/features/$featureName/data/data_source/local_data_source/${featureName}_local_data_source.dart';

@LazySingleton(as: ${camelCaseName}Repository)
class ${camelCaseName}RepositoryImp extends BaseRepositoryImpl implements ${camelCaseName}Repository {
  final ${camelCaseName}RemoteDataSource remoteDataSource;
  final ${camelCaseName}LocalDataSource localDataSource;

  ${camelCaseName}RepositoryImp(this.localDataSource, this.remoteDataSource,
      {required NetworkInfo networkInfo})
      : super(
          baseLocalDataSource: localDataSource,
          networkInfo: networkInfo,
        );
}
  ''';
}
