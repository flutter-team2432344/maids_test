// ignore_for_file: avoid_print
import 'dart:io';

import 'create_base_code.dart';
 
 final currentDirectory = Directory.current;
 final rootName = currentDirectory.uri.pathSegments.last;
void createFeatureFolder(String featureName) {
  final featureDirectory = Directory('./lib/features/$featureName');

  featureDirectory.createSync();
  print('Created feature directory: ${featureDirectory.path}');

  final dataDirectory = Directory('${featureDirectory.path}/data');
  dataDirectory.createSync();
  print('Created data directory: ${dataDirectory.path}');

  for (var subDir in ['model', 'repository', 'data_source']) {
    final subDirPath = '${dataDirectory.path}/$subDir';
    Directory(subDirPath).createSync();
    print('Created subdirectory: $subDirPath');
  }

  for (var subDir in ['remote_data_source', 'local_data_source']) {
    final subDirPath = '${dataDirectory.path}/data_source/$subDir';
    Directory(subDirPath).createSync();

    final dartFilePath = '$subDirPath/${featureName}_$subDir.dart';
    if (subDir == 'remote_data_source') {
      File(dartFilePath).writeAsStringSync(
        generateRemoteClasses(featureName),
      );
    } else {
      File(dartFilePath).writeAsStringSync(
        generateLocalClasses(featureName),
      );
    }
    print('Created Dart file: $dartFilePath');
  }

  final repositoryDirectory = Directory('${dataDirectory.path}/repository');
  repositoryDirectory.createSync();

  final repositoryDartFilePath =
      '${repositoryDirectory.path}/${featureName}_repository_imp.dart';
  File(repositoryDartFilePath).writeAsStringSync(
    generateRepositoryImp(featureName),
  );
  print('Created Dart file: $repositoryDartFilePath');

  final domainDirectory = Directory('${featureDirectory.path}/domain');
  domainDirectory.createSync();
  print('Created domain directory: ${domainDirectory.path}');

  for (var subDir in ['use_cases', 'repository' , 'entity']) {
    final subDirPath = '${domainDirectory.path}/$subDir';
    Directory(subDirPath).createSync();
    print('Created subdirectory: $subDirPath');
  }

  final domainRepositoryDartFilePath =
      '${domainDirectory.path}/repository/${featureName}_repository.dart';
  File(domainRepositoryDartFilePath).writeAsStringSync(
        generateRepository(featureName),
      );
  print('Created Dart file: $domainRepositoryDartFilePath');

  final presentationDirectory =
      Directory('${featureDirectory.path}/presentation');
  presentationDirectory.createSync();
  print('Created presentation directory: ${presentationDirectory.path}');

  for (var subDir in ['bloc', 'screens', 'widgets']) {
    final subDirPath = '${presentationDirectory.path}/$subDir';
    Directory(subDirPath).createSync();
  }

  final blocDirectory = Directory('${presentationDirectory.path}/bloc');
  blocDirectory.createSync();

  final blocDartFilePath = '${blocDirectory.path}/${featureName}_bloc.dart';
  File(blocDartFilePath).writeAsStringSync(generateBlocCode(featureName));
  print('Created Dart file: $blocDartFilePath');

  final eventDartFilePath = '${blocDirectory.path}/${featureName}_event.dart';
  File(eventDartFilePath).writeAsStringSync(generateEventCode(featureName));
  print('Created Dart file: $eventDartFilePath');

  final stateDartFilePath = '${blocDirectory.path}/${featureName}_state.dart';
  File(stateDartFilePath).writeAsStringSync(generateStateCode(featureName));
  print('Created Dart file: $stateDartFilePath');

  print('Feature folder for $featureName created successfully.');
}

void main() {
  print('Enter the name of the feature:');
  final featureName = stdin.readLineSync();

  if (featureName != null && featureName.isNotEmpty) {
    createFeatureFolder(featureName);
  } else {
    print('Invalid feature name. Please provide a non-empty feature name.');
  }
}
