// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i8;
import 'package:get_it/get_it.dart' as _i1;
import 'package:hive/hive.dart' as _i5;
import 'package:injectable/injectable.dart' as _i2;
import 'package:internet_connection_checker/internet_connection_checker.dart'
    as _i7;
import 'package:package_info_plus/package_info_plus.dart' as _i4;
import 'package:shared_preferences/shared_preferences.dart' as _i3;

import 'app/data/data_source/local_data_source/app_local_data_source.dart'
    as _i15;
import 'app/data/repository/app_repository_imp.dart' as _i19;
import 'app/domain/app_repository.dart' as _i18;
import 'app/presentation/bloc/app_bloc.dart' as _i32;
import 'core/data/base_local_data_source.dart' as _i10;
import 'core/data/base_remote_data_source.dart' as _i16;
import 'core/data/base_repository.dart' as _i22;
import 'core/network/network_info.dart' as _i17;
import 'features/auth/data/data_source/local_data_source/auth_local_data_source.dart'
    as _i12;
import 'features/auth/data/data_source/remote_data_source/auth_remote_data_source.dart'
    as _i23;
import 'features/auth/data/repository/auth_repository_imp.dart' as _i34;
import 'features/auth/domain/repository/auth_repository.dart' as _i33;
import 'features/auth/domain/use_cases/sign_in_use_case.dart' as _i36;
import 'features/auth/presentation/bloc/auth_bloc.dart' as _i41;
import 'features/home/data/data_source/local_data_source/home_local_data_source.dart'
    as _i9;
import 'features/home/data/data_source/remote_data_source/home_remote_data_source.dart'
    as _i14;
import 'features/home/data/model/task.dart' as _i6;
import 'features/home/data/repository/home_repository_imp.dart' as _i25;
import 'features/home/domain/repository/home_repository.dart' as _i24;
import 'features/home/domain/use_cases/add_task_use_case.dart' as _i26;
import 'features/home/domain/use_cases/delete_task_use_case.dart' as _i27;
import 'features/home/domain/use_cases/get_app_language_use_case.dart' as _i28;
import 'features/home/domain/use_cases/get_users_use_case.dart' as _i38;
import 'features/home/domain/use_cases/load_tasks_use_case.dart' as _i29;
import 'features/home/domain/use_cases/logout_use_case.dart' as _i39;
import 'features/home/domain/use_cases/set_app_language_use_case.dart' as _i30;
import 'features/home/domain/use_cases/update_task_use_case.dart' as _i31;
import 'features/home/presentation/bloc/home_bloc.dart' as _i40;
import 'features/splash/data/data_source/local_data_source/splash_local_data_source.dart'
    as _i13;
import 'features/splash/data/data_source/remote_data_source/splash_remote_data_source.dart'
    as _i11;
import 'features/splash/data/repository/splash_repository_imp.dart' as _i21;
import 'features/splash/domain/repository/splash_repository.dart' as _i20;
import 'features/splash/domain/use_cases/check_auth_use_case.dart' as _i35;
import 'features/splash/presentation/bloc/splash_bloc.dart' as _i37;
import 'injecation_container.dart' as _i42;

// initializes the registration of main-scope dependencies inside of GetIt
Future<_i1.GetIt> $initGetIt(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) async {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final registerModule = _$RegisterModule();
  await gh.factoryAsync<_i3.SharedPreferences>(
    () => registerModule.prefs,
    preResolve: true,
  );
  await gh.factoryAsync<_i4.PackageInfo>(
    () => registerModule.packageInfo,
    preResolve: true,
  );
  await gh.factoryAsync<_i5.Box<_i6.TaskModel>>(
    () => registerModule.box,
    preResolve: true,
  );
  gh.lazySingleton<_i7.InternetConnectionChecker>(
      () => registerModule.internetConnectionChecker);
  gh.lazySingleton<_i8.Dio>(() => registerModule.dio);
  gh.lazySingleton<_i9.HomeLocalDataSource>(() => _i9.HomeLocalDataSourceImp(
        gh<_i5.Box<_i6.TaskModel>>(),
        sharedPreferences: gh<_i3.SharedPreferences>(),
      ));
  gh.lazySingleton<_i10.BaseLocalDataSource>(() => _i10.BaseLocalDataSourceImp(
      sharedPreferences: gh<_i3.SharedPreferences>()));
  gh.lazySingleton<_i11.SplashRemoteDataSource>(() => _i11.SplashRemoteDataImp(
        dio: gh<_i8.Dio>(),
        localDataSource: gh<_i10.BaseLocalDataSource>(),
      ));
  gh.lazySingleton<_i12.AuthLocalDataSource>(() => _i12.AuthLocalDataSourceImp(
      sharedPreferences: gh<_i3.SharedPreferences>()));
  gh.lazySingleton<_i13.SplashLocalDataSource>(() =>
      _i13.SplashLocalDataSourceImp(
          sharedPreferences: gh<_i3.SharedPreferences>()));
  gh.lazySingleton<_i14.HomeRemoteDataSource>(() => _i14.HomeRemoteDataImp(
        dio: gh<_i8.Dio>(),
        localDataSource: gh<_i10.BaseLocalDataSource>(),
      ));
  gh.lazySingleton<_i15.AppLocalDataSource>(() => _i15.AppLocalDataSourceImp(
      sharedPreferences: gh<_i3.SharedPreferences>()));
  gh.lazySingleton<_i16.BaseRemoteDataSource>(
      () => _i16.BaseRemoteDataSourceImpl(
            dio: gh<_i8.Dio>(),
            localDataSource: gh<_i10.BaseLocalDataSource>(),
          ));
  gh.lazySingleton<_i17.NetworkInfo>(
      () => _i17.NetworkInfoImpl(gh<_i7.InternetConnectionChecker>()));
  gh.lazySingleton<_i18.AppRepository>(() => _i19.AppRepositoryImp(
        gh<_i15.AppLocalDataSource>(),
        baseLocalDataSource: gh<_i10.BaseLocalDataSource>(),
        networkInfo: gh<_i17.NetworkInfo>(),
      ));
  gh.lazySingleton<_i20.SplashRepository>(() => _i21.SplashRepositoryImp(
        gh<_i13.SplashLocalDataSource>(),
        gh<_i11.SplashRemoteDataSource>(),
        networkInfo: gh<_i17.NetworkInfo>(),
      ));
  gh.lazySingleton<_i22.BaseRepository>(() => _i22.BaseRepositoryImpl(
        baseLocalDataSource: gh<_i10.BaseLocalDataSource>(),
        networkInfo: gh<_i17.NetworkInfo>(),
      ));
  gh.lazySingleton<_i23.AuthRemoteDataSource>(() => _i23.AuthRemoteDataImp(
        dio: gh<_i8.Dio>(),
        localDataSource: gh<_i10.BaseLocalDataSource>(),
      ));
  gh.lazySingleton<_i24.HomeRepository>(() => _i25.HomeRepositoryImp(
        gh<_i9.HomeLocalDataSource>(),
        gh<_i14.HomeRemoteDataSource>(),
        networkInfo: gh<_i17.NetworkInfo>(),
      ));
  gh.lazySingleton<_i26.AddTaskUseCase>(
      () => _i26.AddTaskUseCase(gh<_i24.HomeRepository>()));
  gh.lazySingleton<_i27.DeleteTaskUseCase>(
      () => _i27.DeleteTaskUseCase(gh<_i24.HomeRepository>()));
  gh.lazySingleton<_i28.GetLanguageUseCase>(
      () => _i28.GetLanguageUseCase(gh<_i24.HomeRepository>()));
  gh.lazySingleton<_i29.LoadTasksUseCase>(
      () => _i29.LoadTasksUseCase(gh<_i24.HomeRepository>()));
  gh.lazySingleton<_i30.SetAppLanguageUseCase>(
      () => _i30.SetAppLanguageUseCase(gh<_i24.HomeRepository>()));
  gh.lazySingleton<_i31.UpdateTaskUseCase>(
      () => _i31.UpdateTaskUseCase(gh<_i24.HomeRepository>()));
  gh.factory<_i32.AppBloc>(() => _i32.AppBloc(gh<_i18.AppRepository>()));
  gh.lazySingleton<_i33.AuthRepository>(() => _i34.AuthRepositoryImp(
        gh<_i12.AuthLocalDataSource>(),
        gh<_i23.AuthRemoteDataSource>(),
        networkInfo: gh<_i17.NetworkInfo>(),
      ));
  gh.lazySingleton<_i35.CheckAuthUseCase>(
      () => _i35.CheckAuthUseCase(gh<_i20.SplashRepository>()));
  gh.lazySingleton<_i36.SignInUseCase>(
      () => _i36.SignInUseCase(repository: gh<_i33.AuthRepository>()));
  gh.factory<_i37.SplashBloc>(
      () => _i37.SplashBloc(gh<_i35.CheckAuthUseCase>()));
  gh.lazySingleton<_i38.GetUsersUseCase>(
      () => _i38.GetUsersUseCase(repository: gh<_i24.HomeRepository>()));
  gh.lazySingleton<_i39.LogoutUseCase>(
      () => _i39.LogoutUseCase(repository: gh<_i24.HomeRepository>()));
  gh.factory<_i40.HomeBloc>(() => _i40.HomeBloc(
        gh<_i39.LogoutUseCase>(),
        gh<_i28.GetLanguageUseCase>(),
        gh<_i30.SetAppLanguageUseCase>(),
        gh<_i38.GetUsersUseCase>(),
        gh<_i26.AddTaskUseCase>(),
        gh<_i27.DeleteTaskUseCase>(),
        gh<_i31.UpdateTaskUseCase>(),
        gh<_i29.LoadTasksUseCase>(),
      ));
  gh.factory<_i41.AuthBloc>(() => _i41.AuthBloc(gh<_i36.SignInUseCase>()));
  return getIt;
}

class _$RegisterModule extends _i42.RegisterModule {}
