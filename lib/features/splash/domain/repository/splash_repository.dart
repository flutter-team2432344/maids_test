import 'package:dartz/dartz.dart';
import 'package:maids_test/core/error/failures.dart';

abstract class SplashRepository {
  Future<Either<Failure, bool>> checkAuth();
}
