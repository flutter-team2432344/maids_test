import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/splash/domain/repository/splash_repository.dart';
import '../../../../core/error/failures.dart';

@lazySingleton
class CheckAuthUseCase implements UseCase<bool, NoParams> {
  final SplashRepository repository;

  CheckAuthUseCase(this.repository);

  @override
  Future<Either<Failure, bool>> call(NoParams params) async {
    return await repository.checkAuth();
  }
}
