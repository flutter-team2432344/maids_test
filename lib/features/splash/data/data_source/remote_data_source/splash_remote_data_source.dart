  import 'package:injectable/injectable.dart';
  import 'package:maids_test/core/data/base_remote_data_source.dart';

abstract class SplashRemoteDataSource extends BaseRemoteDataSource {
}

@LazySingleton(as: SplashRemoteDataSource)
class SplashRemoteDataImp extends BaseRemoteDataSourceImpl
    implements SplashRemoteDataSource {
  SplashRemoteDataImp({required super.dio, required super.localDataSource});
  }

  