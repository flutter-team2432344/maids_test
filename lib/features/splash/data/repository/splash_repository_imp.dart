import 'package:injectable/injectable.dart';
import 'package:dartz/dartz.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/data/base_repository.dart';
import 'dart:async';
import 'package:maids_test/features/splash/data/data_source/remote_data_source/splash_remote_data_source.dart';
import 'package:maids_test/features/splash/domain/repository/splash_repository.dart';
import 'package:maids_test/features/splash/data/data_source/local_data_source/splash_local_data_source.dart';

@LazySingleton(as: SplashRepository)
class SplashRepositoryImp extends BaseRepositoryImpl
    implements SplashRepository {
  final SplashRemoteDataSource remoteDataSource;
  final SplashLocalDataSource localDataSource;

  SplashRepositoryImp(this.localDataSource, this.remoteDataSource,
      {required super.networkInfo})
      : super(
          baseLocalDataSource: localDataSource,
        );
  @override
  Future<Either<Failure, bool>> checkAuth() async {
    try {
      final token = localDataSource.token;
      if (token.isEmpty) {
        return const Right(false);
      } else {
        return const Right(true);
      }
    } catch (e) {
      return const Left(
        CacheFailure(),
      );
    }
  }
}
