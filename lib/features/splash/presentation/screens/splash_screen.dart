import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:maids_test/core/resources/asset_manager.dart';
import 'package:maids_test/features/auth/presentation/screens/sign_in_screen.dart';
import 'package:maids_test/features/home/presentation/screens/add_task_screen.dart';
import 'package:maids_test/features/home/presentation/screens/home_screen.dart';
import 'package:maids_test/features/splash/presentation/bloc/splash_bloc.dart';
import 'package:maids_test/features/splash/presentation/bloc/splash_state.dart';
import 'package:maids_test/injecation_container.dart';

class SplashScreen extends StatefulWidget {
  static const kPath = "/splash";
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final bloc = sl<SplashBloc>();

  @override
  void initState() {
    bloc.checkAuth();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SplashBloc, SplashState>(
      bloc: bloc,
      listener: (context, state) {
        Future.delayed(const Duration(seconds: 2)).then((value) {
          if (state.success ?? false) {
            state.isAuth!
                ? context.pushReplacement(HomeScreen.kPath)
                : context.pushReplacement(SignInScreen.kPath);
          }
        });
      },
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            automaticallyImplyLeading: false,
          ),
          body: Center(
            child: Image.asset(
              AssetsManager.logo,
              height: 100.h,
              width: 100.w,
            ),
          ),
        );
      },
    );
  }
}
