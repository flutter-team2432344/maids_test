import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/splash/domain/use_cases/check_auth_use_case.dart';
import 'splash_event.dart';
import 'splash_state.dart';

@injectable
class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final CheckAuthUseCase checkAuthUseCase;

  void checkAuth() {
    add(CheckAuthEvent());
  }

  void clearError() {
    add(ClearError());
  }

  @factoryMethod
  SplashBloc(this.checkAuthUseCase) : super(SplashState.initial()) {
    on<SplashEvent>((event, emit) async {
      /// *** Clear Error ***
      if (event is ClearError) {
        emit(
          state.rebuild(
            (b) => b..message = '',
          ),
        );
      }

      /// **** CHECK AUTH **** ///
      if (event is CheckAuthEvent) {
        final result = await checkAuthUseCase(NoParams());
        result.fold(
          (failure) => emit(
            state.rebuild(
              (b) => b
                ..isAuth = false
                ..success = false,
            ),
          ),
          (isAuth) => emit(
            state.rebuild(
              (b) => b
                ..isAuth = isAuth
                ..success = true,
            ),
          ),
        );
      }
    });
  }
}
