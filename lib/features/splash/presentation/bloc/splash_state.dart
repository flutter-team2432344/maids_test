import 'package:built_value/built_value.dart';
import '../../../../core/base_states/base_state.dart';

part 'splash_state.g.dart';

abstract class SplashState
    with BaseState
    implements Built<SplashState, SplashStateBuilder> {
  bool? get isAuth;
  bool? get success;
  SplashState._();

  factory SplashState([Function(SplashStateBuilder b) updates]) = _$SplashState;
  factory SplashState.initial() {
    return SplashState(
      (b) => b
        ..isLoading = false
        ..isAuth = null
        ..error = false
        ..success = null
        ..message = '',
    );
  }
}
