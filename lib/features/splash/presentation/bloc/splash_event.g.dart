// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'splash_event.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder)? updates]) =>
      (new ClearErrorBuilder()..update(updates))._build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper(r'ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError? _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ClearError build() => _build();

  _$ClearError _build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$CheckAuthEvent extends CheckAuthEvent {
  factory _$CheckAuthEvent([void Function(CheckAuthEventBuilder)? updates]) =>
      (new CheckAuthEventBuilder()..update(updates))._build();

  _$CheckAuthEvent._() : super._();

  @override
  CheckAuthEvent rebuild(void Function(CheckAuthEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CheckAuthEventBuilder toBuilder() =>
      new CheckAuthEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CheckAuthEvent;
  }

  @override
  int get hashCode {
    return 49097513;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper(r'CheckAuthEvent').toString();
  }
}

class CheckAuthEventBuilder
    implements Builder<CheckAuthEvent, CheckAuthEventBuilder> {
  _$CheckAuthEvent? _$v;

  CheckAuthEventBuilder();

  @override
  void replace(CheckAuthEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CheckAuthEvent;
  }

  @override
  void update(void Function(CheckAuthEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  CheckAuthEvent build() => _build();

  _$CheckAuthEvent _build() {
    final _$result = _$v ?? new _$CheckAuthEvent._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
