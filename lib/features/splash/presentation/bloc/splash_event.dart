import 'package:built_value/built_value.dart';

part 'splash_event.g.dart';

abstract class SplashEvent {}

abstract class ClearError extends SplashEvent
    implements Built<ClearError, ClearErrorBuilder> {
  ClearError._();

  factory ClearError([Function(ClearErrorBuilder b) updates]) = _$ClearError;

  factory ClearError.initial() {
    return ClearError((b) => b);
  }
}

abstract class CheckAuthEvent extends SplashEvent
    implements Built<CheckAuthEvent, CheckAuthEventBuilder> {
  CheckAuthEvent._();

  factory CheckAuthEvent([Function(CheckAuthEventBuilder b) updates]) = _$CheckAuthEvent;

  factory CheckAuthEvent.initial() {
    return CheckAuthEvent((b) => b);
  }
}