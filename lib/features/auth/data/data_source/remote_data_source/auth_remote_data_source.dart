import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/data/base_remote_data_source.dart';
import 'package:maids_test/core/error/exceptions.dart';
import 'package:maids_test/core/error/status_code_handler.dart';
import 'package:maids_test/core/models/base_response_model.dart';
import 'package:maids_test/core/utils/constants.dart';
import 'package:maids_test/core/utils/end_points.dart';
import 'package:maids_test/features/auth/domain/use_cases/sign_in_use_case.dart';

abstract class AuthRemoteDataSource extends BaseRemoteDataSource {
  Future<BaseResponseModel?> signIn({
    required SignInParams params,
  });
}

@LazySingleton(as: AuthRemoteDataSource)
class AuthRemoteDataImp extends BaseRemoteDataSourceImpl
    implements AuthRemoteDataSource {
  AuthRemoteDataImp({required super.dio, required super.localDataSource});

  @override
  Future<BaseResponseModel?> signIn({
    required SignInParams params,
  }) async {
    {
      debugPrint("performPostRequest\n");
      String? token = localDataSource.token;

      try {
        debugPrint("Sending request...\n");
        debugPrint("token is $token \n");
        final response = await dio.post(
          EndPoints.signIn,
          data: {
            "email": params.email,
            "password": params.password,
          },
          options: GetOptions.getOptionsWithToken(
            token,
          ),
        );

        debugPrint("Decoding response...\n");
        final result = BaseResponseModel.fromJson(
          json.decode(response.data),
          (json) => json,
        );

        debugPrint("Switching result\n");

        if (result.token != null || result.token != '') {
          debugPrint("Result is true\n");
          return result;
        } else {
          debugPrint("Result is false\n");
          throw ServerException(
              error: result.message ?? ErrorMessage.somethingWentWrong.tr());
        }
      } on DioException catch (e) {
        debugPrint("Dio Error :(\n");

        if (e.response != null) {
          debugPrint("Dio error and the response is not null\n");
          throw statusCodeHandler(e.response!);
        } else {
          debugPrint("Dio error and the response is null\n");
          throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
        }
      } catch (e) {
        if (e is HandledException) {
          rethrow;
        } else if (e is ParseException) {
          debugPrint(
            "\n*** Failed to parse the model please check the base or base list response model ***\n",
          );
          throw HandledException(error: ErrorMessage.somethingWentWrong.tr());
        } else {
          debugPrint("Request fail and the error not handled\n");
          throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
        }
      }
    }
  }
  // performPostRequest<BaseResponseModel?>(
  //   endpoint: EndPoints.signIn,
  //   data: {
  //     "email": params.email,
  //     "password": params.password,
  //   },
  // );
}
