import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/utils/constants.dart';
import 'package:maids_test/core/data/base_local_data_source.dart';

abstract class AuthLocalDataSource extends BaseLocalDataSource {
  Future<void> storeToken(String? token);
}

@LazySingleton(as: AuthLocalDataSource)
class AuthLocalDataSourceImp extends BaseLocalDataSourceImp
    implements AuthLocalDataSource {
  AuthLocalDataSourceImp({required super.sharedPreferences});
  @override
  Future<void> storeToken(String? token) async {
    sharedPreferences
        .setString(LocalStorageKeys.apiToken, token ?? '')
        .then((value) {
      debugPrint('token store : $token');
      debugPrint('token store success');
    });
  }
}
