import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:dartz/dartz.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/data/base_repository.dart';
import 'package:maids_test/core/models/base_response_model.dart';
import 'dart:async';
import 'package:maids_test/features/auth/data/data_source/remote_data_source/auth_remote_data_source.dart';
import 'package:maids_test/features/auth/domain/repository/auth_repository.dart';
import 'package:maids_test/features/auth/data/data_source/local_data_source/auth_local_data_source.dart';
import 'package:maids_test/features/auth/domain/use_cases/sign_in_use_case.dart';

@LazySingleton(as: AuthRepository)
class AuthRepositoryImp extends BaseRepositoryImpl implements AuthRepository {
  final AuthRemoteDataSource remoteDataSource;
  final AuthLocalDataSource localDataSource;

  AuthRepositoryImp(this.localDataSource, this.remoteDataSource,
      {required super.networkInfo})
      : super(
          baseLocalDataSource: localDataSource,
        );

  @override
  Future<Either<Failure, BaseResponseModel?>> signIn(
          {required SignInParams params}) async =>
      await requestWithTokenAndLanguage<BaseResponseModel?>(
        (_, language) async {
          final result = await remoteDataSource.signIn(params: params);
          debugPrint('response in repo : $result');
          localDataSource.storeToken(result?.token ?? '');
          return result;
        },
      );
}
