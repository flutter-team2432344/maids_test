import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:go_router/go_router.dart';
import 'package:maids_test/core/resources/asset_manager.dart';
import 'package:maids_test/core/resources/string_manager.dart';
import 'package:maids_test/core/resources/theme_manager.dart';
import 'package:maids_test/core/widgets/base_buttons.dart';
import 'package:maids_test/core/widgets/base_field.dart';
import 'package:maids_test/core/widgets/my_loader.dart';
import 'package:maids_test/features/auth/domain/use_cases/sign_in_use_case.dart';
import 'package:maids_test/features/auth/presentation/bloc/auth_bloc.dart';
import 'package:maids_test/features/auth/presentation/bloc/auth_state.dart';
import 'package:maids_test/features/home/presentation/screens/home_screen.dart';
import 'package:maids_test/injecation_container.dart';

class SignInScreen extends StatefulWidget {
  static const kPath = "/sign_in";
  const SignInScreen({super.key});

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final GlobalKey<FormBuilderState> formKey = GlobalKey<FormBuilderState>();
  final bloc = sl<AuthBloc>();
  bool isSecure = true;
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
      bloc: bloc,
      listener: (context, state) {
        if (state.message.isNotEmpty) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(state.message),
              backgroundColor: Colors.red,
            ),
          );
        }
        if (state.userEntity?.token != null) {
          context.pushReplacement(HomeScreen.kPath);
        }
      },
      builder: (context, state) {
        bloc.clearError();
        return Scaffold(
          body: Padding(
            padding: EdgeInsets.only(top: 40.h),
            child: SingleChildScrollView(
              child: FormBuilder(
                key: formKey,
                child: Center(
                  child: Column(
                    children: [
                      Image.asset(
                        AssetsManager.logo,
                        height: 100.h,
                        width: 100.w,
                      ),
                      SizedBox(height: 39.h),
                      Text(
                        StringManager.signIn.tr(),
                        style: TextStyle(
                          color: ColorManager.primary,
                          fontSize: 30.sp,
                        ),
                      ),
                      SizedBox(
                        height: 29.w,
                      ),
                      Text(
                        StringManager.signInMessage.tr(),
                        style: TextStyle(
                          fontSize: 15.sp,
                        ),
                      ),
                      SizedBox(
                        height: 39.h,
                      ),
                      BaseTextFormField(
                        hintText: StringManager.email.tr(),
                        controller: emailController,
                        prefixIcon: const Icon(
                          Icons.email,
                          color: ColorManager.primary,
                        ),
                        textInputType: TextInputType.emailAddress,
                        validator: FormBuilderValidators.required(),
                      ),
                      SizedBox(
                        height: 28.h,
                      ),
                      BaseTextFormField(
                        hintText: StringManager.password.tr(),
                        controller: passwordController,
                        prefixIcon: const Icon(
                          Icons.lock,
                          color: ColorManager.primary,
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            bloc.passwordVisibility(
                              passwordSecure: isSecure,
                            );
                          },
                          icon: Icon(
                            state.passwordSecure == true
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                        ),
                        obscureText: state.passwordSecure,
                        validator: FormBuilderValidators.minLength(8),
                      ),
                      SizedBox(height: 30.h),
                      state.isLoading == true
                          ? const MyLoader()
                          : BaseButton(
                              buttonText: StringManager.signIn.tr(),
                              height: 51.h,
                              width: 307.w,
                              onPressed: () {
                                if (formKey.currentState?.saveAndValidate() ??
                                    false) {
                                  bloc.signIn(
                                    params: SignInParams(
                                      email: emailController.text,
                                      password: passwordController.text,
                                    ),
                                  );
                                }
                              },
                            ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
