import 'package:built_value/built_value.dart';
import 'package:maids_test/core/models/base_response_model.dart';
import '../../../../core/base_states/base_state.dart';

part 'auth_state.g.dart';

abstract class AuthState
    with BaseState
    implements Built<AuthState, AuthStateBuilder> {
  bool get passwordSecure;
  BaseResponseModel? get userEntity;
  AuthState._();

  factory AuthState([Function(AuthStateBuilder b) updates]) = _$AuthState;
  factory AuthState.initial() {
    return AuthState(
      (b) => b
        ..isLoading = false
        ..error = false
        ..passwordSecure = true
        ..userEntity = null
        ..message = '',
    );
  }
}
