// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AuthState extends AuthState {
  @override
  final bool passwordSecure;
  @override
  final BaseResponseModel<dynamic>? userEntity;
  @override
  final bool isLoading;
  @override
  final bool error;
  @override
  final String message;

  factory _$AuthState([void Function(AuthStateBuilder)? updates]) =>
      (new AuthStateBuilder()..update(updates))._build();

  _$AuthState._(
      {required this.passwordSecure,
      this.userEntity,
      required this.isLoading,
      required this.error,
      required this.message})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        passwordSecure, r'AuthState', 'passwordSecure');
    BuiltValueNullFieldError.checkNotNull(isLoading, r'AuthState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(error, r'AuthState', 'error');
    BuiltValueNullFieldError.checkNotNull(message, r'AuthState', 'message');
  }

  @override
  AuthState rebuild(void Function(AuthStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AuthStateBuilder toBuilder() => new AuthStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AuthState &&
        passwordSecure == other.passwordSecure &&
        userEntity == other.userEntity &&
        isLoading == other.isLoading &&
        error == other.error &&
        message == other.message;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, passwordSecure.hashCode);
    _$hash = $jc(_$hash, userEntity.hashCode);
    _$hash = $jc(_$hash, isLoading.hashCode);
    _$hash = $jc(_$hash, error.hashCode);
    _$hash = $jc(_$hash, message.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AuthState')
          ..add('passwordSecure', passwordSecure)
          ..add('userEntity', userEntity)
          ..add('isLoading', isLoading)
          ..add('error', error)
          ..add('message', message))
        .toString();
  }
}

class AuthStateBuilder implements Builder<AuthState, AuthStateBuilder> {
  _$AuthState? _$v;

  bool? _passwordSecure;
  bool? get passwordSecure => _$this._passwordSecure;
  set passwordSecure(bool? passwordSecure) =>
      _$this._passwordSecure = passwordSecure;

  BaseResponseModel<dynamic>? _userEntity;
  BaseResponseModel<dynamic>? get userEntity => _$this._userEntity;
  set userEntity(BaseResponseModel<dynamic>? userEntity) =>
      _$this._userEntity = userEntity;

  bool? _isLoading;
  bool? get isLoading => _$this._isLoading;
  set isLoading(bool? isLoading) => _$this._isLoading = isLoading;

  bool? _error;
  bool? get error => _$this._error;
  set error(bool? error) => _$this._error = error;

  String? _message;
  String? get message => _$this._message;
  set message(String? message) => _$this._message = message;

  AuthStateBuilder();

  AuthStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _passwordSecure = $v.passwordSecure;
      _userEntity = $v.userEntity;
      _isLoading = $v.isLoading;
      _error = $v.error;
      _message = $v.message;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AuthState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AuthState;
  }

  @override
  void update(void Function(AuthStateBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AuthState build() => _build();

  _$AuthState _build() {
    final _$result = _$v ??
        new _$AuthState._(
            passwordSecure: BuiltValueNullFieldError.checkNotNull(
                passwordSecure, r'AuthState', 'passwordSecure'),
            userEntity: userEntity,
            isLoading: BuiltValueNullFieldError.checkNotNull(
                isLoading, r'AuthState', 'isLoading'),
            error: BuiltValueNullFieldError.checkNotNull(
                error, r'AuthState', 'error'),
            message: BuiltValueNullFieldError.checkNotNull(
                message, r'AuthState', 'message'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
