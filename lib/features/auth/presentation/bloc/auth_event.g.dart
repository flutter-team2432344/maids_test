// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_event.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder)? updates]) =>
      (new ClearErrorBuilder()..update(updates))._build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper(r'ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError? _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ClearError build() => _build();

  _$ClearError _build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$PasswordVisibilityEvent extends PasswordVisibilityEvent {
  @override
  final bool? passwordSecure;

  factory _$PasswordVisibilityEvent(
          [void Function(PasswordVisibilityEventBuilder)? updates]) =>
      (new PasswordVisibilityEventBuilder()..update(updates))._build();

  _$PasswordVisibilityEvent._({this.passwordSecure}) : super._();

  @override
  PasswordVisibilityEvent rebuild(
          void Function(PasswordVisibilityEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PasswordVisibilityEventBuilder toBuilder() =>
      new PasswordVisibilityEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PasswordVisibilityEvent &&
        passwordSecure == other.passwordSecure;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, passwordSecure.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'PasswordVisibilityEvent')
          ..add('passwordSecure', passwordSecure))
        .toString();
  }
}

class PasswordVisibilityEventBuilder
    implements
        Builder<PasswordVisibilityEvent, PasswordVisibilityEventBuilder> {
  _$PasswordVisibilityEvent? _$v;

  bool? _passwordSecure;
  bool? get passwordSecure => _$this._passwordSecure;
  set passwordSecure(bool? passwordSecure) =>
      _$this._passwordSecure = passwordSecure;

  PasswordVisibilityEventBuilder();

  PasswordVisibilityEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _passwordSecure = $v.passwordSecure;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PasswordVisibilityEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PasswordVisibilityEvent;
  }

  @override
  void update(void Function(PasswordVisibilityEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  PasswordVisibilityEvent build() => _build();

  _$PasswordVisibilityEvent _build() {
    final _$result =
        _$v ?? new _$PasswordVisibilityEvent._(passwordSecure: passwordSecure);
    replace(_$result);
    return _$result;
  }
}

class _$SignInEvent extends SignInEvent {
  @override
  final SignInParams params;

  factory _$SignInEvent([void Function(SignInEventBuilder)? updates]) =>
      (new SignInEventBuilder()..update(updates))._build();

  _$SignInEvent._({required this.params}) : super._() {
    BuiltValueNullFieldError.checkNotNull(params, r'SignInEvent', 'params');
  }

  @override
  SignInEvent rebuild(void Function(SignInEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SignInEventBuilder toBuilder() => new SignInEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SignInEvent && params == other.params;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, params.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SignInEvent')..add('params', params))
        .toString();
  }
}

class SignInEventBuilder implements Builder<SignInEvent, SignInEventBuilder> {
  _$SignInEvent? _$v;

  SignInParams? _params;
  SignInParams? get params => _$this._params;
  set params(SignInParams? params) => _$this._params = params;

  SignInEventBuilder();

  SignInEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _params = $v.params;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SignInEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SignInEvent;
  }

  @override
  void update(void Function(SignInEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SignInEvent build() => _build();

  _$SignInEvent _build() {
    final _$result = _$v ??
        new _$SignInEvent._(
            params: BuiltValueNullFieldError.checkNotNull(
                params, r'SignInEvent', 'params'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
