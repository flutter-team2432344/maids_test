import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/features/auth/domain/use_cases/sign_in_use_case.dart';
import 'auth_event.dart';
import 'auth_state.dart';

@injectable
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final SignInUseCase signInUseCase;
  void passwordVisibility({
    required bool? passwordSecure,
  }) {
    add(
      PasswordVisibilityEvent(
        (b) => b..passwordSecure = passwordSecure,
      ),
    );
  }

  void signIn({required SignInParams params}) {
    add(
      SignInEvent(
        (b) => b..params = params,
      ),
    );
  }

  void clearError() {
    add(ClearError());
  }

  @factoryMethod
  AuthBloc(this.signInUseCase) : super(AuthState.initial()) {
    on<AuthEvent>((event, emit) async {
      /// *** Clear Error ***
      if (event is ClearError) {
        emit(
          state.rebuild(
            (b) => b..message = '',
          ),
        );
      }

      /// *** password visibility ***
      if (event is PasswordVisibilityEvent) {
        emit(
          state.rebuild(
            (b) => b..passwordSecure = !state.passwordSecure,
          ),
        );
      }

      /// *** Sign In ***
      if (event is SignInEvent) {
        emit(
          state.rebuild(
            (b) => b..isLoading = true,
          ),
        );
        final result = await signInUseCase(
          SignInParams(
            email: event.params.email,
            password: event.params.password,
          ),
        );
        result.fold(
          (failure) => emit(
            state.rebuild(
              (b) => b
                ..isLoading = false
                ..error = true
                ..message = failure.error,
            ),
          ),
          (userEntity) => emit(
            state.rebuild(
              (b) => b
                ..isLoading = false
                ..userEntity = userEntity,
            ),
          ),
        );
      }
    });
  }
}
