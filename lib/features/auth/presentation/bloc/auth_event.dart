import 'package:built_value/built_value.dart';
import 'package:maids_test/features/auth/domain/use_cases/sign_in_use_case.dart';

part 'auth_event.g.dart';

abstract class AuthEvent {}

abstract class ClearError extends AuthEvent
    implements Built<ClearError, ClearErrorBuilder> {
  ClearError._();

  factory ClearError([Function(ClearErrorBuilder b) updates]) = _$ClearError;

  factory ClearError.initial() {
    return ClearError((b) => b);
  }
}

abstract class PasswordVisibilityEvent extends AuthEvent
    implements Built<PasswordVisibilityEvent, PasswordVisibilityEventBuilder> {
  bool? get passwordSecure;
  PasswordVisibilityEvent._();
  factory PasswordVisibilityEvent(
          [void Function(PasswordVisibilityEventBuilder b) update]) =
      _$PasswordVisibilityEvent;
}

abstract class SignInEvent extends AuthEvent
    implements Built<SignInEvent, SignInEventBuilder> {
  SignInParams get params;
  SignInEvent._();
  factory SignInEvent([void Function(SignInEventBuilder b) update]) =
      _$SignInEvent;
}
  