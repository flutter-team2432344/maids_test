import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/models/base_response_model.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/auth/domain/repository/auth_repository.dart';

@lazySingleton
class SignInUseCase extends UseCase<BaseResponseModel?, SignInParams> {
  final AuthRepository repository;

  SignInUseCase({required this.repository});
  @override
  Future<Either<Failure, BaseResponseModel?>> call(SignInParams params) async =>
      await repository.signIn(params: params);
}

class SignInParams extends Equatable {
  final String? email;
  final String? password;

  const SignInParams({
    required this.email,
    required this.password,
  });
  @override
  List<Object?> get props => [];
}
