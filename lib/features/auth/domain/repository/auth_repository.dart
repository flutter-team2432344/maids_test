import 'package:dartz/dartz.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/models/base_response_model.dart';
import 'package:maids_test/features/auth/domain/use_cases/sign_in_use_case.dart';

abstract class AuthRepository {
  Future<Either<Failure, BaseResponseModel?>> signIn({
    required SignInParams params,
  });
}
