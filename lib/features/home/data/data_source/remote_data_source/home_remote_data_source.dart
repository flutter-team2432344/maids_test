import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/data/base_remote_data_source.dart';
import 'package:maids_test/core/error/exceptions.dart';
import 'package:maids_test/core/models/paginate_response_model.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/core/utils/constants.dart';
import 'package:maids_test/core/utils/end_points.dart';
import 'package:maids_test/features/home/data/model/user_model.dart';

abstract class HomeRemoteDataSource extends BaseRemoteDataSource {
  Future<PaginatedResponse<UserModel>> getUsers({
    required PaginationParams params,
    required String token,
  });
}

@LazySingleton(as: HomeRemoteDataSource)
class HomeRemoteDataImp extends BaseRemoteDataSourceImpl
    implements HomeRemoteDataSource {
  HomeRemoteDataImp({required super.dio, required super.localDataSource});

  @override
  Future<PaginatedResponse<UserModel>> getUsers({
    required PaginationParams params,
    required String token,
  }) async {
    final response = await dio.get(
      EndPoints.getUsers(params.page),
      options: GetOptions.getOptionsWithToken(token),
    );
    if (response.statusCode == 200) {
      final result = PaginatedResponse<UserModel>.fromJson(
        json.decode(response.data),
        (item) => UserModel.fromJson(item),
      );
      return result;
    } else {
      throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
    }
  }
}
