import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/data/base_local_data_source.dart';
import 'package:maids_test/core/error/exceptions.dart';
import 'package:maids_test/core/resources/string_manager.dart';
import 'package:maids_test/core/utils/dev.dart';
import 'package:maids_test/features/home/data/model/task.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:hive_flutter/adapters.dart';

abstract class HomeLocalDataSource extends BaseLocalDataSource {
  Future<List<TaskModel>> getTasks(String userToken);
  Future<String> addTask(String userToken, TaskModel task);
  Future<String> deleteTask(String userToken, String id);
  Future<String> updateTask(TaskModel task);
}

@LazySingleton(as: HomeLocalDataSource)
class HomeLocalDataSourceImp extends BaseLocalDataSourceImp
    implements HomeLocalDataSource {
  final Box<TaskModel> box;

  HomeLocalDataSourceImp(this.box, {required super.sharedPreferences});

  @override
  Future<List<TaskModel>> getTasks(String userToken) async {
    try {
      final List<TaskModel> allTasks = box.values.toList();
      return allTasks.where((task) => task.userToken == userToken).toList();
    } catch (_) {
      throw DatabaseException(message: StringManager.errorAddTask.tr());
    }
  }

  @override
  Future<String> addTask(String userToken, TaskModel task) async {
    try {
      task.userToken = userToken;
      await box.put(task.id, task);
      return StringManager.successAddTask.tr();
    } catch (_) {
      throw DatabaseException(message: StringManager.errorAddTask.tr());
    }
  }

  @override
  Future<String> deleteTask(String userToken, String id) async {
    try {
      await box.delete(id);
      return StringManager.successDeleteTask.tr();
    } catch (_) {
      throw DatabaseException(message: StringManager.errorDeleteTask.tr());
    }
  }

  @override
  Future<String> updateTask(TaskModel task) async {
    try {
      task.isCompleted = !task.isCompleted;
      await task.save();
      return StringManager.successUpdateTask.tr();
    } catch (e) {
      Dev.console([e]);
      throw DatabaseException(message: StringManager.errorUpdateTask.tr());
    }
  }
}
