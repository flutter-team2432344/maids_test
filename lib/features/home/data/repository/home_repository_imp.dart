import 'package:injectable/injectable.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:maids_test/core/entities/paginate_list.dart';
import 'package:maids_test/core/error/exceptions.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/data/base_repository.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/home/data/data_source/remote_data_source/home_remote_data_source.dart';
import 'package:maids_test/features/home/data/model/task.dart';
import 'package:maids_test/features/home/domain/entity/user_entity.dart';
import 'package:maids_test/features/home/domain/repository/home_repository.dart';
import 'package:maids_test/features/home/data/data_source/local_data_source/home_local_data_source.dart';

@LazySingleton(as: HomeRepository)
class HomeRepositoryImp extends BaseRepositoryImpl implements HomeRepository {
  final HomeRemoteDataSource remoteDataSource;
  final HomeLocalDataSource localDataSource;

  HomeRepositoryImp(this.localDataSource, this.remoteDataSource,
      {required super.networkInfo})
      : super(
          baseLocalDataSource: localDataSource,
        );
  @override
  Future<Either<Failure, void>> logout() async {
    try {
      await localDataSource.logout();
      return const Right(null);
    } catch (e) {
      debugPrint('Logout error in repository, error is : $e');
      return const Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, String>> getAppLanguage() async {
    try {
      final language = localDataSource.getAppLanguage();
      return Right(language);
    } catch (e) {
      debugPrint("error in get app language, error is : $e");
      return const Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, void>> setAppLanguage(String language) async {
    try {
      await localDataSource.setAppLanguage(language);
      return const Right(null);
    } catch (e) {
      debugPrint('error in set app language, error is : $e');
      return const Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, PaginateList<UserEntity>>> getUsers({
    required PaginationParams params,
  }) async =>
      await requestWithTokenAndLanguage<PaginateList<UserEntity>>(
          (token, language) async {
        final paginateModel = await remoteDataSource.getUsers(
          token: token,
          params: params,
        );
        return PaginateList<UserEntity>(
          data: paginateModel.data,
          page: paginateModel.page,
          count: paginateModel.lastPage,
        );
      });

  @override
  Future<Either<Failure, List<TaskModel>>> loadTasks() async {
    try {
      final result = await localDataSource.getTasks(localDataSource.token);
      return Right(result);
    } on DatabaseException catch (failure) {
      return left(DatabaseFailure(failure.message));
    }
  }

  @override
  Future<Either<Failure, String>> addTask(TaskModel task) async {
    try {
      final result = await localDataSource.addTask(localDataSource.token, task);
      return Right(result);
    } on DatabaseException catch (failure) {
      return left(DatabaseFailure(failure.message));
    }
  }

  @override
  Future<Either<Failure, String>> deleteTask(String id) async {
    try {
      final result =
          await localDataSource.deleteTask(localDataSource.token, id);
      return Right(result);
    } on DatabaseException catch (failure) {
      return left(DatabaseFailure(failure.message));
    }
  }

  @override
  Future<Either<Failure, String>> updateTask(TaskModel task) async {
    try {
      final result = await localDataSource.updateTask(task);
      return Right(result);
    } on DatabaseException catch (failure) {
      return left(DatabaseFailure(failure.message));
    }
  }
}
