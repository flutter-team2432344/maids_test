import 'package:hive/hive.dart';
import 'package:maids_test/core/local_data_source/hive_constants.dart';
import 'package:uuid/uuid.dart';

part 'task.g.dart';

@HiveType(typeId: HiveConstants.todoItem)
class TaskModel extends HiveObject {
  @HiveField(0)
  final String id;
  @HiveField(1)
  String title;
  @HiveField(2)
  String subtitle;
  @HiveField(3)
  final DateTime createdAtTime;
  @HiveField(4)
  final DateTime createdAtDate;
  @HiveField(5)
  bool isCompleted;
  @HiveField(6)
  String userToken;
  TaskModel({
    required this.id,
    required this.title,
    required this.subtitle,
    required this.createdAtTime,
    required this.createdAtDate,
    required this.isCompleted,
    required this.userToken,
  });

  factory TaskModel.create({
    required String title,
    required String subtitle,
    String? userToken,
    DateTime? createdAtTime,
    DateTime? createdAtDate,
  }) =>
      TaskModel(
        id: const Uuid().v1(),
        title: title,
        subtitle: subtitle,
        createdAtTime: DateTime.now(),
        isCompleted: false,
        createdAtDate: DateTime.now(),
        userToken: userToken ?? '',
      );
}
