import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/home/data/model/task.dart';
import 'package:maids_test/features/home/domain/repository/home_repository.dart';

@lazySingleton
class LoadTasksUseCase implements UseCase<List<TaskModel>, NoParams> {
  final HomeRepository repository;

  LoadTasksUseCase(this.repository);

  @override
  Future<Either<Failure, List<TaskModel>>> call(NoParams params) async {
    return await repository.loadTasks();
  }
}