import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/home/domain/repository/home_repository.dart';

@lazySingleton
class SetAppLanguageUseCase implements UseCase<void, ParamsSetLanguageUseCase> {
  final HomeRepository repository;

  SetAppLanguageUseCase(this.repository);

  @override
  Future<Either<Failure, void>> call(ParamsSetLanguageUseCase params) async {
    return await repository.setAppLanguage(params.language);
  }
}

class ParamsSetLanguageUseCase {
  final String language;

  ParamsSetLanguageUseCase(this.language);
}
