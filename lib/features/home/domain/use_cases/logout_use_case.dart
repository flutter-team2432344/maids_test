import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/home/domain/repository/home_repository.dart';

@lazySingleton
class LogoutUseCase implements UseCase<void, NoParams> {
  final HomeRepository repository;

  LogoutUseCase({
    required this.repository,
  });
  @override
  Future<Either<Failure, void>> call(NoParams params) async =>
      await repository.logout();
}
