import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/home/domain/repository/home_repository.dart';

@lazySingleton
class GetLanguageUseCase implements UseCase<String, NoParams> {
  final HomeRepository repository;

  GetLanguageUseCase(this.repository);

  @override
  Future<Either<Failure, String>> call(NoParams params) async {
    return await repository.getAppLanguage();
  }
}
