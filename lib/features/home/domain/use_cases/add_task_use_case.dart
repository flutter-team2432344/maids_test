import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/home/data/model/task.dart';
import 'package:maids_test/features/home/domain/repository/home_repository.dart';

@lazySingleton
class AddTaskUseCase implements UseCase<void, TaskModel> {
  final HomeRepository repository;

  AddTaskUseCase(this.repository);

  @override
  Future<Either<Failure, void>> call(TaskModel task) async {
    return await repository.addTask(task);
  }
}
