import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/home/domain/repository/home_repository.dart';

@lazySingleton
class DeleteTaskUseCase implements UseCase<void, IDParams> {
  final HomeRepository repository;

  DeleteTaskUseCase(this.repository);

  @override
  Future<Either<Failure, void>> call(IDParams params) async {
    return await repository.deleteTask(params.id);
  }
}