import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/entities/paginate_list.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/home/domain/entity/user_entity.dart';
import 'package:maids_test/features/home/domain/repository/home_repository.dart';

@lazySingleton
class GetUsersUseCase
    implements UseCase<PaginateList<UserEntity>, PaginationParams> {
  final HomeRepository repository;

  GetUsersUseCase({required this.repository});

  @override
  Future<Either<Failure, PaginateList<UserEntity>>> call(PaginationParams params) async =>
      await repository.getUsers(params: params);
}
