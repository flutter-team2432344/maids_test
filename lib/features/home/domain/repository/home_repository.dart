import 'package:dartz/dartz.dart';
import 'package:maids_test/core/entities/paginate_list.dart';
import 'package:maids_test/core/error/failures.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/home/data/model/task.dart';
import 'package:maids_test/features/home/domain/entity/user_entity.dart';

abstract class HomeRepository {
  Future<Either<Failure, void>> logout();
  Future<Either<Failure, void>> setAppLanguage(String language);
  Future<Either<Failure, String>> getAppLanguage();
  Future<Either<Failure, PaginateList<UserEntity>>> getUsers({
    required PaginationParams params,
  });
  Future<Either<Failure, List<TaskModel>>> loadTasks();
  Future<Either<Failure, String>> addTask(TaskModel task);
  Future<Either<Failure, String>> deleteTask(String id);
  Future<Either<Failure, String>> updateTask(TaskModel task);
}
