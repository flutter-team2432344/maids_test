import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:maids_test/core/resources/app_theme.dart';
import 'package:maids_test/core/resources/string_manager.dart';
import 'package:maids_test/core/resources/theme_manager.dart';
import 'package:maids_test/features/home/presentation/bloc/home_bloc.dart';
import 'package:maids_test/features/home/presentation/bloc/home_state.dart';
import 'package:maids_test/features/home/presentation/screens/home_screen.dart';
import 'package:maids_test/injecation_container.dart';

class BuildMiddleTextFieldsAddTask extends StatefulWidget {
  const BuildMiddleTextFieldsAddTask({super.key});

  @override
  State<BuildMiddleTextFieldsAddTask> createState() =>
      _BuildMiddleTextFieldsAddTaskState();
}

class _BuildMiddleTextFieldsAddTaskState
    extends State<BuildMiddleTextFieldsAddTask> {
  final FocusNode focusNode = FocusNode();
  Color color = Colors.grey;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final TextEditingController titleController = TextEditingController();
  final TextEditingController subtitleController = TextEditingController();

  final bloc = sl<HomeBloc>();
  @override
  void initState() {
    focusNode.addListener(_changeColor);
    super.initState();
  }

  _changeColor() {
    if (focusNode.hasFocus) {
      color = ColorManager.primary;
    } else {
      color = Colors.grey;
    }
    setState(() {});
  }

  @override
  void dispose() {
    focusNode.removeListener(_changeColor);
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      bloc: bloc,
      listener: (context, state) {
        if (state.statusAddTask.isSuccess) {
          context.pushReplacement(HomeScreen.kPath);
        }
      },
      child: Form(
        key: formKey,
        child: SizedBox(
          width: double.infinity,
          height: ScreenUtil().screenHeight / 2,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: REdgeInsetsDirectional.only(start: 30),
                child: Text(
                  StringManager.whatAreYouPlaning.tr(),
                  style: textTheme.headlineMedium,
                ),
              ),
              Container(
                width: ScreenUtil().screenWidth,
                margin: REdgeInsets.symmetric(horizontal: 16),
                child: ListTile(
                  title: TextFormField(
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "This field is required";
                      }
                      return null;
                    },
                    controller: titleController,
                    maxLines: 6,
                    minLines: 1,
                    keyboardType: TextInputType.text,
                    cursorHeight: 25,
                    style:
                        textTheme.headlineMedium!.copyWith(color: Colors.black),
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey.shade300),
                      ),
                    ),
                  ),
                ),
              ),
              10.verticalSpace,
              Container(
                width: ScreenUtil().screenWidth,
                margin: REdgeInsets.symmetric(horizontal: 16),
                child: ListTile(
                  title: TextFormField(
                    focusNode: focusNode,
                    controller: subtitleController,
                    style: const TextStyle(color: Colors.black),
                    maxLines: 6,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      prefixIcon: const Icon(
                        Icons.bookmark_border_outlined,
                      ),
                      prefixIconColor: color,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.r),
                        borderSide: BorderSide(color: Colors.grey.shade300),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.r),
                        borderSide: BorderSide(color: Colors.grey.shade300),
                      ),
                      hintText: StringManager.addNote.tr(),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: REdgeInsets.only(top: 10.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.r),
                      ),
                      minWidth: 150.w,
                      height: 55.h,
                      onPressed: () {
                        if (formKey.currentState!.validate()) {
                          debugPrint('1111 : ${titleController.text}');
                          debugPrint('2222 : ${subtitleController.text}');
                          bloc.addTask(
                            title: titleController.text,
                            subtitle: subtitleController.text,
                          );
                        }
                      },
                      color: ColorManager.primary,
                      child: Text(
                        StringManager.addTask.tr(),
                        style: textTheme.headlineSmall!.copyWith(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
