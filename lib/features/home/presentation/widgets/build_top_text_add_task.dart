import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:maids_test/core/resources/app_theme.dart';
import 'package:maids_test/core/resources/string_manager.dart';

class BuildTopTextAddTask extends StatelessWidget {
  const BuildTopTextAddTask({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 100.h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Flexible(
            child: Divider(
              thickness: 2,
            ),
          ),
          5.horizontalSpace,
          RichText(
            text: TextSpan(
              text: StringManager.addNewTask.tr(),
              style: textTheme.titleLarge,
              children: const [
                TextSpan(
                  text: ' Task',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                  ),
                )
              ],
            ),
          ),
          5.horizontalSpace,
          const Flexible(
            child: Divider(
              thickness: 2,
            ),
          ),
        ],
      ),
    );
  }
}
