import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:lottie/lottie.dart';
import 'package:maids_test/core/resources/app_theme.dart';
import 'package:maids_test/core/resources/asset_manager.dart';
import 'package:maids_test/core/resources/string_manager.dart';
import 'package:maids_test/core/resources/theme_manager.dart';
import 'package:maids_test/features/home/presentation/bloc/home_bloc.dart';
import 'package:maids_test/features/home/presentation/bloc/home_state.dart';
import 'package:animate_do/animate_do.dart';
import 'package:maids_test/features/home/presentation/widgets/item_task_widget.dart';
import 'package:maids_test/injecation_container.dart';

class NewTaskScreen extends StatefulWidget {
  static const kPath = '/new_tasks';

  const NewTaskScreen({super.key});

  @override
  State<NewTaskScreen> createState() => _NewTaskScreenState();
}

class _NewTaskScreenState extends State<NewTaskScreen> {
  final bloc = sl<HomeBloc>();
  @override
  void initState() {
    bloc.loadTasks();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocBuilder<HomeBloc, HomeState>(
          bloc: bloc,
          builder: (context, state) {
            return SizedBox(
              width: double.infinity,
              height: double.infinity,
              child: Column(
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: 100.h,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Flexible(
                          child: Divider(
                            thickness: 2,
                          ),
                        ),
                        5.horizontalSpace,
                        Text(
                          StringManager.newTasks.tr(),
                          style: textTheme.displayLarge,
                        ),
                        5.horizontalSpace,
                        const Flexible(
                          child: Divider(
                            thickness: 2,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: state.newTasks.isNotEmpty
                        ? ListView.builder(
                            physics: const BouncingScrollPhysics(),
                            itemCount: state.newTasks.length,
                            itemBuilder: (BuildContext context, int index) {
                              var task = state.newTasks[index];
                              return Dismissible(
                                direction: DismissDirection.horizontal,
                                background: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Icon(
                                      Icons.delete_outline,
                                      color: Colors.grey,
                                    ),
                                    8.horizontalSpace,
                                    Text(
                                      StringManager.thisTaskWasDeleted.tr(),
                                      style: const TextStyle(
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ],
                                ),
                                onDismissed: (direction) {
                                  bloc.deleteTask(id: task.id);
                                },
                                key: Key(task.id),
                                child: TaskItemWidget(
                                  task: task,
                                ),
                              );
                            },
                          )
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              FadeIn(
                                child: SizedBox(
                                  width: 200.w,
                                  height: 200.h,
                                  child: Lottie.asset(
                                    AssetsManager.emty,
                                    animate:
                                        state.tasks.isNotEmpty ? false : true,
                                  ),
                                ),
                              ),
                              FadeInUp(
                                from: 30,
                                child: Text(
                                  StringManager.thereIsNoNewTasks.tr(),
                                  style: textTheme.headlineMedium!.copyWith(
                                    color: ColorManager.black,
                                  ),
                                ),
                              ),
                            ],
                          ),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
