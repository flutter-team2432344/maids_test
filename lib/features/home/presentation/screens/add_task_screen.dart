import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:maids_test/features/home/presentation/bloc/home_bloc.dart';
import 'package:maids_test/features/home/presentation/widgets/build_middle_text_fields_add_task.dart';
import 'package:maids_test/features/home/presentation/widgets/build_top_text_add_task.dart';
import 'package:maids_test/injecation_container.dart';

class AddTaskScreen extends StatefulWidget {
  static const kPath = "/add_task";
  const AddTaskScreen({super.key});

  @override
  State<AddTaskScreen> createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  final bloc = sl<HomeBloc>();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () => context.pop(),
            icon: const Icon(
              Icons.arrow_back_ios_new_outlined,
              size: 30,
            ),
          ),
        ),
        body: const SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                BuildTopTextAddTask(),
                BuildMiddleTextFieldsAddTask(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
