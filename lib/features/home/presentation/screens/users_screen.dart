import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:maids_test/core/widgets/my_loader.dart';
import 'package:maids_test/features/home/presentation/bloc/home_bloc.dart';
import 'package:maids_test/features/home/presentation/bloc/home_state.dart';
import 'package:maids_test/features/home/presentation/widgets/user_card.dart';
import 'package:maids_test/injecation_container.dart';

class UsersScreen extends StatefulWidget {
  static const kPath = "/home";
  const UsersScreen({super.key});

  @override
  State<UsersScreen> createState() => _UsersScreenState();
}

class _UsersScreenState extends State<UsersScreen> {
  final bloc = sl<HomeBloc>();
  final controller = ScrollController();

  @override
  void initState() {
    super.initState();
    controller.addListener(scrollListener);
    bloc.getUsers();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeBloc, HomeState>(
      bloc: bloc,
      listener: (context, state) {},
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.white,
          ),
          body: SingleChildScrollView(
            controller: controller,
            child: Column(
              children: [
                SizedBox(
                  height: 60.h,
                ),
                GridView.builder(
                  physics: const BouncingScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 40.h,
                    crossAxisSpacing: 5.w,
                  ),
                  itemCount: state.users?.length ?? 0,
                  itemBuilder: (BuildContext context, int index) {
                    final user = state.users?[index];
                    return UserCard(
                      user: user,
                    );
                  },
                ),
                state.isDataFinished == false
                    ? state.isLoading
                        ? const Center(child: MyLoader())
                        : const MyLoader()
                    : const SizedBox(),
                SizedBox(
                  height: 20.h,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void scrollListener() {
    if (controller.hasClients &&
        controller.position.extentAfter == 0.0 &&
        !bloc.state.isDataFinished &&
        !bloc.state.paginateLoad &&
        !bloc.state.isLoading) {
      bloc.getUsers();
    }
  }

  @override
  void dispose() {
    controller.removeListener(scrollListener);
    super.dispose();
  }
}
