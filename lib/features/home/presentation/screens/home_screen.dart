import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:animate_do/animate_do.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:lottie/lottie.dart';
import 'package:maids_test/core/resources/app_theme.dart';
import 'package:maids_test/core/resources/asset_manager.dart';
import 'package:maids_test/core/resources/string_manager.dart';
import 'package:maids_test/core/resources/theme_manager.dart';
import 'package:maids_test/core/widgets/base_drawer.dart';
import 'package:maids_test/features/home/presentation/bloc/home_bloc.dart';
import 'package:maids_test/features/home/presentation/bloc/home_state.dart';
import 'package:maids_test/features/home/presentation/screens/add_task_screen.dart';
import 'package:maids_test/features/home/presentation/widgets/item_task_widget.dart';
import 'package:maids_test/injecation_container.dart';

class HomeScreen extends StatefulWidget {
  static const kPath = '/home_screen';

  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final bloc = sl<HomeBloc>();

  @override
  void initState() {
    bloc.loadTasks();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const BaseDrawer(),
      appBar: AppBar(
        elevation: 0.0,
      ),
      body: BlocBuilder<HomeBloc, HomeState>(
        bloc: bloc,
        builder: (context, state) {
          return SizedBox(
            width: double.infinity,
            height: double.infinity,
            child: Column(
              children: [
                Container(
                  margin: REdgeInsets.fromLTRB(55, 0, 0, 0),
                  width: double.infinity,
                  height: 100.h,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      RSizedBox(
                        width: 25,
                        height: 25,
                        child: CircularProgressIndicator(
                          valueColor: const AlwaysStoppedAnimation(
                            ColorManager.primary,
                          ),
                          backgroundColor: Colors.grey,
                          strokeWidth: 5,
                          value: state.doneTasks.length /
                              state.valueOfTheIndicator,
                        ),
                      ),
                      25.horizontalSpace,
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            StringManager.myTasks.tr(),
                            style: textTheme.displayLarge,
                          ),
                          5.verticalSpace,
                          Text(
                            "${state.doneTasks.length} of ${state.allTasks.length} task",
                            style: textTheme.titleMedium,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: REdgeInsetsDirectional.only(top: 10),
                  child: const Divider(
                    thickness: 2,
                    color: Colors.grey,
                    indent: 100,
                  ),
                ),
                Expanded(
                  child: state.allTasks.isNotEmpty
                      ? ListView.builder(
                          physics: const BouncingScrollPhysics(),
                          itemCount: state.allTasks.length,
                          itemBuilder: (BuildContext context, int index) {
                            var task = state.allTasks[index];
                            return Dismissible(
                              direction: DismissDirection.horizontal,
                              background: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Icon(
                                    Icons.delete_outline,
                                    color: Colors.grey,
                                  ),
                                  8.horizontalSpace,
                                  Text(
                                    StringManager.thisTaskWasDeleted.tr(),
                                    style: const TextStyle(
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                              onDismissed: (direction) {
                                bloc.deleteTask(id: task.id);
                              },
                              key: Key(task.id),
                              child: TaskItemWidget(
                                task: task,
                              ),
                            );
                          },
                        )
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            FadeIn(
                              child: SizedBox(
                                width: 200.w,
                                height: 200.h,
                                child: Lottie.asset(
                                  AssetsManager.emty,
                                  animate:
                                      state.tasks.isNotEmpty ? false : true,
                                ),
                              ),
                            ),
                            FadeInUp(
                              from: 30,
                              child: Text(
                                StringManager.thereIsNoTasks.tr(),
                                style: textTheme.headlineMedium!.copyWith(
                                  color: ColorManager.black,
                                ),
                              ),
                            ),
                          ],
                        ),
                )
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => context.push(AddTaskScreen.kPath),
        tooltip: StringManager.addTask.tr(),
        child: const Icon(Icons.add),
      ),
    );
  }
}
