// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$HomeState extends HomeState {
  @override
  final bool? logout;
  @override
  final String appLanguage;
  @override
  final BuiltList<UserEntity>? users;
  @override
  final bool isDataFinished;
  @override
  final bool paginateLoad;
  @override
  final int newDataPage;
  @override
  final BuiltList<TaskModel> tasks;
  @override
  final RequestState statusGetTasks;
  @override
  final String errorAddTask;
  @override
  final RequestStateInitial statusAddTask;
  @override
  final String errorDeleteTask;
  @override
  final RequestStateInitial statusDeleteTask;
  @override
  final String errorUpdateTask;
  @override
  final RequestStateInitial statusUpdateTask;
  @override
  final bool isLoading;
  @override
  final bool error;
  @override
  final String message;

  factory _$HomeState([void Function(HomeStateBuilder)? updates]) =>
      (new HomeStateBuilder()..update(updates))._build();

  _$HomeState._(
      {this.logout,
      required this.appLanguage,
      this.users,
      required this.isDataFinished,
      required this.paginateLoad,
      required this.newDataPage,
      required this.tasks,
      required this.statusGetTasks,
      required this.errorAddTask,
      required this.statusAddTask,
      required this.errorDeleteTask,
      required this.statusDeleteTask,
      required this.errorUpdateTask,
      required this.statusUpdateTask,
      required this.isLoading,
      required this.error,
      required this.message})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        appLanguage, r'HomeState', 'appLanguage');
    BuiltValueNullFieldError.checkNotNull(
        isDataFinished, r'HomeState', 'isDataFinished');
    BuiltValueNullFieldError.checkNotNull(
        paginateLoad, r'HomeState', 'paginateLoad');
    BuiltValueNullFieldError.checkNotNull(
        newDataPage, r'HomeState', 'newDataPage');
    BuiltValueNullFieldError.checkNotNull(tasks, r'HomeState', 'tasks');
    BuiltValueNullFieldError.checkNotNull(
        statusGetTasks, r'HomeState', 'statusGetTasks');
    BuiltValueNullFieldError.checkNotNull(
        errorAddTask, r'HomeState', 'errorAddTask');
    BuiltValueNullFieldError.checkNotNull(
        statusAddTask, r'HomeState', 'statusAddTask');
    BuiltValueNullFieldError.checkNotNull(
        errorDeleteTask, r'HomeState', 'errorDeleteTask');
    BuiltValueNullFieldError.checkNotNull(
        statusDeleteTask, r'HomeState', 'statusDeleteTask');
    BuiltValueNullFieldError.checkNotNull(
        errorUpdateTask, r'HomeState', 'errorUpdateTask');
    BuiltValueNullFieldError.checkNotNull(
        statusUpdateTask, r'HomeState', 'statusUpdateTask');
    BuiltValueNullFieldError.checkNotNull(isLoading, r'HomeState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(error, r'HomeState', 'error');
    BuiltValueNullFieldError.checkNotNull(message, r'HomeState', 'message');
  }

  @override
  HomeState rebuild(void Function(HomeStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HomeStateBuilder toBuilder() => new HomeStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HomeState &&
        logout == other.logout &&
        appLanguage == other.appLanguage &&
        users == other.users &&
        isDataFinished == other.isDataFinished &&
        paginateLoad == other.paginateLoad &&
        newDataPage == other.newDataPage &&
        tasks == other.tasks &&
        statusGetTasks == other.statusGetTasks &&
        errorAddTask == other.errorAddTask &&
        statusAddTask == other.statusAddTask &&
        errorDeleteTask == other.errorDeleteTask &&
        statusDeleteTask == other.statusDeleteTask &&
        errorUpdateTask == other.errorUpdateTask &&
        statusUpdateTask == other.statusUpdateTask &&
        isLoading == other.isLoading &&
        error == other.error &&
        message == other.message;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, logout.hashCode);
    _$hash = $jc(_$hash, appLanguage.hashCode);
    _$hash = $jc(_$hash, users.hashCode);
    _$hash = $jc(_$hash, isDataFinished.hashCode);
    _$hash = $jc(_$hash, paginateLoad.hashCode);
    _$hash = $jc(_$hash, newDataPage.hashCode);
    _$hash = $jc(_$hash, tasks.hashCode);
    _$hash = $jc(_$hash, statusGetTasks.hashCode);
    _$hash = $jc(_$hash, errorAddTask.hashCode);
    _$hash = $jc(_$hash, statusAddTask.hashCode);
    _$hash = $jc(_$hash, errorDeleteTask.hashCode);
    _$hash = $jc(_$hash, statusDeleteTask.hashCode);
    _$hash = $jc(_$hash, errorUpdateTask.hashCode);
    _$hash = $jc(_$hash, statusUpdateTask.hashCode);
    _$hash = $jc(_$hash, isLoading.hashCode);
    _$hash = $jc(_$hash, error.hashCode);
    _$hash = $jc(_$hash, message.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'HomeState')
          ..add('logout', logout)
          ..add('appLanguage', appLanguage)
          ..add('users', users)
          ..add('isDataFinished', isDataFinished)
          ..add('paginateLoad', paginateLoad)
          ..add('newDataPage', newDataPage)
          ..add('tasks', tasks)
          ..add('statusGetTasks', statusGetTasks)
          ..add('errorAddTask', errorAddTask)
          ..add('statusAddTask', statusAddTask)
          ..add('errorDeleteTask', errorDeleteTask)
          ..add('statusDeleteTask', statusDeleteTask)
          ..add('errorUpdateTask', errorUpdateTask)
          ..add('statusUpdateTask', statusUpdateTask)
          ..add('isLoading', isLoading)
          ..add('error', error)
          ..add('message', message))
        .toString();
  }
}

class HomeStateBuilder implements Builder<HomeState, HomeStateBuilder> {
  _$HomeState? _$v;

  bool? _logout;
  bool? get logout => _$this._logout;
  set logout(bool? logout) => _$this._logout = logout;

  String? _appLanguage;
  String? get appLanguage => _$this._appLanguage;
  set appLanguage(String? appLanguage) => _$this._appLanguage = appLanguage;

  ListBuilder<UserEntity>? _users;
  ListBuilder<UserEntity> get users =>
      _$this._users ??= new ListBuilder<UserEntity>();
  set users(ListBuilder<UserEntity>? users) => _$this._users = users;

  bool? _isDataFinished;
  bool? get isDataFinished => _$this._isDataFinished;
  set isDataFinished(bool? isDataFinished) =>
      _$this._isDataFinished = isDataFinished;

  bool? _paginateLoad;
  bool? get paginateLoad => _$this._paginateLoad;
  set paginateLoad(bool? paginateLoad) => _$this._paginateLoad = paginateLoad;

  int? _newDataPage;
  int? get newDataPage => _$this._newDataPage;
  set newDataPage(int? newDataPage) => _$this._newDataPage = newDataPage;

  ListBuilder<TaskModel>? _tasks;
  ListBuilder<TaskModel> get tasks =>
      _$this._tasks ??= new ListBuilder<TaskModel>();
  set tasks(ListBuilder<TaskModel>? tasks) => _$this._tasks = tasks;

  RequestState? _statusGetTasks;
  RequestState? get statusGetTasks => _$this._statusGetTasks;
  set statusGetTasks(RequestState? statusGetTasks) =>
      _$this._statusGetTasks = statusGetTasks;

  String? _errorAddTask;
  String? get errorAddTask => _$this._errorAddTask;
  set errorAddTask(String? errorAddTask) => _$this._errorAddTask = errorAddTask;

  RequestStateInitial? _statusAddTask;
  RequestStateInitial? get statusAddTask => _$this._statusAddTask;
  set statusAddTask(RequestStateInitial? statusAddTask) =>
      _$this._statusAddTask = statusAddTask;

  String? _errorDeleteTask;
  String? get errorDeleteTask => _$this._errorDeleteTask;
  set errorDeleteTask(String? errorDeleteTask) =>
      _$this._errorDeleteTask = errorDeleteTask;

  RequestStateInitial? _statusDeleteTask;
  RequestStateInitial? get statusDeleteTask => _$this._statusDeleteTask;
  set statusDeleteTask(RequestStateInitial? statusDeleteTask) =>
      _$this._statusDeleteTask = statusDeleteTask;

  String? _errorUpdateTask;
  String? get errorUpdateTask => _$this._errorUpdateTask;
  set errorUpdateTask(String? errorUpdateTask) =>
      _$this._errorUpdateTask = errorUpdateTask;

  RequestStateInitial? _statusUpdateTask;
  RequestStateInitial? get statusUpdateTask => _$this._statusUpdateTask;
  set statusUpdateTask(RequestStateInitial? statusUpdateTask) =>
      _$this._statusUpdateTask = statusUpdateTask;

  bool? _isLoading;
  bool? get isLoading => _$this._isLoading;
  set isLoading(bool? isLoading) => _$this._isLoading = isLoading;

  bool? _error;
  bool? get error => _$this._error;
  set error(bool? error) => _$this._error = error;

  String? _message;
  String? get message => _$this._message;
  set message(String? message) => _$this._message = message;

  HomeStateBuilder();

  HomeStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _logout = $v.logout;
      _appLanguage = $v.appLanguage;
      _users = $v.users?.toBuilder();
      _isDataFinished = $v.isDataFinished;
      _paginateLoad = $v.paginateLoad;
      _newDataPage = $v.newDataPage;
      _tasks = $v.tasks.toBuilder();
      _statusGetTasks = $v.statusGetTasks;
      _errorAddTask = $v.errorAddTask;
      _statusAddTask = $v.statusAddTask;
      _errorDeleteTask = $v.errorDeleteTask;
      _statusDeleteTask = $v.statusDeleteTask;
      _errorUpdateTask = $v.errorUpdateTask;
      _statusUpdateTask = $v.statusUpdateTask;
      _isLoading = $v.isLoading;
      _error = $v.error;
      _message = $v.message;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HomeState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$HomeState;
  }

  @override
  void update(void Function(HomeStateBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  HomeState build() => _build();

  _$HomeState _build() {
    _$HomeState _$result;
    try {
      _$result = _$v ??
          new _$HomeState._(
              logout: logout,
              appLanguage: BuiltValueNullFieldError.checkNotNull(
                  appLanguage, r'HomeState', 'appLanguage'),
              users: _users?.build(),
              isDataFinished: BuiltValueNullFieldError.checkNotNull(
                  isDataFinished, r'HomeState', 'isDataFinished'),
              paginateLoad: BuiltValueNullFieldError.checkNotNull(
                  paginateLoad, r'HomeState', 'paginateLoad'),
              newDataPage: BuiltValueNullFieldError.checkNotNull(
                  newDataPage, r'HomeState', 'newDataPage'),
              tasks: tasks.build(),
              statusGetTasks: BuiltValueNullFieldError.checkNotNull(
                  statusGetTasks, r'HomeState', 'statusGetTasks'),
              errorAddTask: BuiltValueNullFieldError.checkNotNull(
                  errorAddTask, r'HomeState', 'errorAddTask'),
              statusAddTask: BuiltValueNullFieldError.checkNotNull(
                  statusAddTask, r'HomeState', 'statusAddTask'),
              errorDeleteTask: BuiltValueNullFieldError.checkNotNull(
                  errorDeleteTask, r'HomeState', 'errorDeleteTask'),
              statusDeleteTask: BuiltValueNullFieldError.checkNotNull(statusDeleteTask, r'HomeState', 'statusDeleteTask'),
              errorUpdateTask: BuiltValueNullFieldError.checkNotNull(errorUpdateTask, r'HomeState', 'errorUpdateTask'),
              statusUpdateTask: BuiltValueNullFieldError.checkNotNull(statusUpdateTask, r'HomeState', 'statusUpdateTask'),
              isLoading: BuiltValueNullFieldError.checkNotNull(isLoading, r'HomeState', 'isLoading'),
              error: BuiltValueNullFieldError.checkNotNull(error, r'HomeState', 'error'),
              message: BuiltValueNullFieldError.checkNotNull(message, r'HomeState', 'message'));
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'users';
        _users?.build();

        _$failedField = 'tasks';
        tasks.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'HomeState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
