import 'package:built_value/built_value.dart';
import 'package:maids_test/features/home/data/model/task.dart';
part 'home_event.g.dart';

abstract class HomeEvent {}

abstract class ClearError extends HomeEvent
    implements Built<ClearError, ClearErrorBuilder> {
  ClearError._();

  factory ClearError([Function(ClearErrorBuilder b) updates]) = _$ClearError;

  factory ClearError.initial() {
    return ClearError((b) => b);
  }
}

abstract class LogoutEvent extends HomeEvent
    implements Built<LogoutEvent, LogoutEventBuilder> {
  LogoutEvent._();
  factory LogoutEvent([void Function(LogoutEventBuilder b) updates]) =
      _$LogoutEvent;
}

abstract class SetAppLanguage extends HomeEvent
    implements Built<SetAppLanguage, SetAppLanguageBuilder> {
  String get language;

  SetAppLanguage._();

  factory SetAppLanguage([Function(SetAppLanguageBuilder b) updates]) =
      _$SetAppLanguage;

  factory SetAppLanguage.initial() {
    return SetAppLanguage((b) => b);
  }
}

abstract class AddTaskEvent extends HomeEvent
    implements Built<AddTaskEvent, AddTaskEventBuilder> {
  String get title;
  String? get subTitle;
  AddTaskEvent._();

  factory AddTaskEvent([Function(AddTaskEventBuilder b) updates]) =
      _$AddTaskEvent;

  factory AddTaskEvent.initial() {
    return AddTaskEvent((b) => b);
  }
}

abstract class UpdateTaskEvent extends HomeEvent
    implements Built<UpdateTaskEvent, UpdateTaskEventBuilder> {
  TaskModel get task;

  UpdateTaskEvent._();

  factory UpdateTaskEvent([Function(UpdateTaskEventBuilder b) updates]) =
      _$UpdateTaskEvent;

  factory UpdateTaskEvent.initial() {
    return UpdateTaskEvent((b) => b);
  }
}

abstract class LoadTasksEvent extends HomeEvent
    implements Built<LoadTasksEvent, LoadTasksEventBuilder> {
  LoadTasksEvent._();

  factory LoadTasksEvent([Function(LoadTasksEventBuilder b) updates]) =
      _$LoadTasksEvent;

  factory LoadTasksEvent.initial() {
    return LoadTasksEvent((b) => b);
  }
}

abstract class DeleteTaskEvent extends HomeEvent
    implements Built<DeleteTaskEvent, DeleteTaskEventBuilder> {
  String get taskId;

  DeleteTaskEvent._();

  factory DeleteTaskEvent([Function(DeleteTaskEventBuilder b) updates]) =
      _$DeleteTaskEvent;

  factory DeleteTaskEvent.initial() {
    return DeleteTaskEvent((b) => b);
  }
}

abstract class GetAppLanguage extends HomeEvent
    implements Built<GetAppLanguage, GetAppLanguageBuilder> {
  GetAppLanguage._();

  factory GetAppLanguage([Function(GetAppLanguageBuilder b) updates]) =
      _$GetAppLanguage;

  factory GetAppLanguage.initial() {
    return GetAppLanguage((b) => b);
  }
}

abstract class GetUsersEvent extends HomeEvent
    implements Built<GetUsersEvent, GetUsersEventBuilder> {
  int get page;

  GetUsersEvent._();

  factory GetUsersEvent([Function(GetUsersEventBuilder b) updates]) =
      _$GetUsersEvent;

  factory GetUsersEvent.initial() {
    return GetUsersEvent((b) => b);
  }
}
