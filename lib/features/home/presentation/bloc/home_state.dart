import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:maids_test/core/base_states/request_state.dart';
import 'package:maids_test/features/home/data/model/task.dart';
import 'package:maids_test/features/home/domain/entity/user_entity.dart';
import '../../../../core/base_states/base_state.dart';

part 'home_state.g.dart';

abstract class HomeState
    with BaseState
    implements Built<HomeState, HomeStateBuilder> {
  bool? get logout;

  String get appLanguage;

  BuiltList<UserEntity>? get users;

  bool get isDataFinished;

  bool get paginateLoad;

  int get newDataPage;

  BuiltList<TaskModel> get tasks;

  List<TaskModel> get allTasks => tasks.toList()
    ..sort((a, b) => b.createdAtDate.compareTo(a.createdAtDate));
  List<TaskModel> get doneTasks =>
      tasks.toList().where((element) => element.isCompleted).toList();
  List<TaskModel> get newTasks =>
      tasks.toList().where((element) => !element.isCompleted).toList();

  RequestState get statusGetTasks;

  String get errorAddTask;
  RequestStateInitial get statusAddTask;

  String get errorDeleteTask;
  RequestStateInitial get statusDeleteTask;

  String get errorUpdateTask;
  RequestStateInitial get statusUpdateTask;
  //
  int get valueOfTheIndicator => tasks.isNotEmpty ? tasks.length : 3;

  HomeState._();

  factory HomeState([Function(HomeStateBuilder b) updates]) = _$HomeState;
  factory HomeState.initial() {
    return HomeState(
      (b) => b
        ..logout = null
        ..appLanguage = 'en'
        ..statusGetTasks = RequestState.loading
        ..errorAddTask = ""
        ..statusAddTask = RequestStateInitial.initial
        ..errorDeleteTask = ""
        ..statusDeleteTask = RequestStateInitial.initial
        ..errorUpdateTask = ""
        ..statusUpdateTask = RequestStateInitial.initial
        ..users.replace([])
        ..newDataPage = 1
        ..isDataFinished = false
        ..paginateLoad = false
        ..isLoading = false
        ..error = false
        ..message = '',
    );
  }
}
