import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/core/base_states/request_state.dart';
import 'package:maids_test/core/use_case/use_case.dart';
import 'package:maids_test/features/home/data/model/task.dart';
import 'package:maids_test/features/home/domain/use_cases/add_task_use_case.dart';
import 'package:maids_test/features/home/domain/use_cases/delete_task_use_case.dart';
import 'package:maids_test/features/home/domain/use_cases/get_app_language_use_case.dart';
import 'package:maids_test/features/home/domain/use_cases/get_users_use_case.dart';
import 'package:maids_test/features/home/domain/use_cases/load_tasks_use_case.dart';
import 'package:maids_test/features/home/domain/use_cases/logout_use_case.dart';
import 'package:maids_test/features/home/domain/use_cases/set_app_language_use_case.dart';
import 'package:maids_test/features/home/domain/use_cases/update_task_use_case.dart';
import 'home_event.dart';
import 'home_state.dart';

@injectable
class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final LogoutUseCase logoutUseCase;
  final GetLanguageUseCase getLanguageUseCase;
  final SetAppLanguageUseCase setAppLanguageUseCase;
  final GetUsersUseCase getUsersUseCase;
  final AddTaskUseCase addTaskUseCase;
  final DeleteTaskUseCase deleteTaskUseCase;
  final UpdateTaskUseCase updateTaskUseCase;
  final LoadTasksUseCase loadTasksUseCase;

  void logout() {
    add(LogoutEvent());
  }

  void setAppLanguage(String language) {
    add(SetAppLanguage((b) => b..language = language));
  }

  void getAppLanguage() {
    add(GetAppLanguage());
  }

  void clearError() {
    add(ClearError());
  }

  void getUsers() {
    add(
      GetUsersEvent(
        (b) => b..page = state.newDataPage,
      ),
    );
  }

  void loadTasks() {
    add(LoadTasksEvent());
  }

  void addTask({
    required String title,
    required String? subtitle,
  }) {
    add(
      AddTaskEvent(
        (b) => b
          ..title = title
          ..subTitle = subtitle,
      ),
    );
  }

  void updateTask({required TaskModel task}) {
    add(
      UpdateTaskEvent(
        (b) => b..task = task,
      ),
    );
  }

  void deleteTask({required String id}) {
    add(
      DeleteTaskEvent(
        (b) => b..taskId = id,
      ),
    );
  }

  @factoryMethod
  HomeBloc(
    this.logoutUseCase,
    this.getLanguageUseCase,
    this.setAppLanguageUseCase,
    this.getUsersUseCase,
    this.addTaskUseCase,
    this.deleteTaskUseCase,
    this.updateTaskUseCase,
    this.loadTasksUseCase,
  ) : super(HomeState.initial()) {
    on<HomeEvent>((event, emit) async {
      /// *** Clear Error *** ///
      if (event is ClearError) {
        emit(
          state.rebuild(
            (b) => b..message = '',
          ),
        );
      }

      /// *** Logout ***///
      if (event is LogoutEvent) {
        final result = await logoutUseCase(NoParams());
        result.fold(
          (failure) => emit(
            state.rebuild(
              (b) => b..logout = false,
            ),
          ),
          (logout) => emit(state.rebuild(
            (b) => b..logout = true,
          )),
        );
      }

      /// *** set language *** ///
      if (event is SetAppLanguage) {
        final result = await setAppLanguageUseCase(
          ParamsSetLanguageUseCase(event.language),
        );
        result.fold(
          (failure) => emit(state.rebuild((b) => b)),
          (color) => emit(
            state.rebuild((b) => b),
          ),
        );
      }

      /// *** get language *** ///
      if (event is GetAppLanguage) {
        emit(state.rebuild((b) => b..isLoading = true));
        final result = await getLanguageUseCase(NoParams());
        result.fold(
          (failure) => emit(state.rebuild(
            (b) => b
              ..isLoading = false
              ..message = failure.error.toString(),
          )),
          (language) {
            emit(state.rebuild(
              (b) => b
                ..isLoading = false
                ..appLanguage = language
                ..message = "",
            ));
          },
        );
      }

      /// *** Get Store Users ***
      if (event is GetUsersEvent) {
        if (!state.isDataFinished) {
          if (state.newDataPage == 1) {
            emit(
              state.rebuild(
                (b) => b
                  ..isLoading = true
                  ..paginateLoad = true
                  ..message = '',
              ),
            );
          } else {
            emit(
              state.rebuild(
                (b) => b
                  ..paginateLoad = true
                  ..message = '',
              ),
            );
          }
          final result = await getUsersUseCase(
            PaginationParams(
              page: event.page,
            ),
          );
          result.fold(
            (failure) => emit(
              state.rebuild(
                (b) => b
                  ..isLoading = false
                  ..error = true
                  ..message = failure.error,
              ),
            ),
            (users) => emit(
              state.rebuild(
                (b) => b
                  ..users.addAll(users.data)
                  ..isDataFinished = users.page == users.count
                  ..newDataPage = users.page + 1
                  ..paginateLoad = false
                  ..isLoading = false,
              ),
            ),
          );
        }
      }

      /// *** Load Task *** ///
      if (event is LoadTasksEvent) {
        final result = await loadTasksUseCase(NoParams());
        result.fold(
          (l) => emit(
            state.rebuild(
              (b) => b
                ..message = l.error
                ..statusGetTasks = RequestState.error,
            ),
          ),
          (r) => emit(
            state.rebuild(
              (b) => b
                ..tasks.replace(r)
                ..statusGetTasks = RequestState.success,
            ),
          ),
        );
      }

      /// *** Add Task *** ///
      if (event is AddTaskEvent) {
        emit(state
            .rebuild((b) => b..statusAddTask = RequestStateInitial.loading));
        TaskModel task = TaskModel.create(
          title: event.title,
          subtitle: event.subTitle != null ? event.subTitle! : "",
        );
        final result = await addTaskUseCase(task);
        result.fold(
          (l) => emit(
            state.rebuild(
              (b) => b
                ..errorAddTask = l.error
                ..statusAddTask = RequestStateInitial.error,
            ),
          ),
          (r) => emit(
            state.rebuild(
              (b) => b
                ..statusAddTask = RequestStateInitial.success
                ..tasks.add(task),
            ),
          ),
        );
      }

      /// *** Delete Task *** ///
      if (event is DeleteTaskEvent) {
        emit(
          state
              .rebuild((b) => b.statusDeleteTask = RequestStateInitial.initial),
        );
        final result = await deleteTaskUseCase(IDParams(id: event.taskId));
        result.fold(
          (l) => emit(
            state.rebuild(
              (b) => b
                ..errorDeleteTask = l.error
                ..statusDeleteTask = RequestStateInitial.error,
            ),
          ),
          (r) => emit(
            state.rebuild(
              (b) => b
                ..statusDeleteTask = RequestStateInitial.success
                ..tasks.removeWhere(
                  (e) => e.id == event.taskId,
                ),
            ),
          ),
        );
      }

      /// *** Update Task *** ///
      if (event is UpdateTaskEvent) {
        final result = await updateTaskUseCase(event.task);
        result.fold(
          (l) => emit(state.rebuild(
            (b) => b
              ..statusUpdateTask = RequestStateInitial.error
              ..errorUpdateTask = l.error,
          )),
          (r) {
            emit(
              state.rebuild(
                (b) => b.statusUpdateTask = RequestStateInitial.success,
              ),
            );
          },
        );
      }
    });
  }
}
