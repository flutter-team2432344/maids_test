// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_event.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder)? updates]) =>
      (new ClearErrorBuilder()..update(updates))._build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper(r'ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError? _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ClearError build() => _build();

  _$ClearError _build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$LogoutEvent extends LogoutEvent {
  factory _$LogoutEvent([void Function(LogoutEventBuilder)? updates]) =>
      (new LogoutEventBuilder()..update(updates))._build();

  _$LogoutEvent._() : super._();

  @override
  LogoutEvent rebuild(void Function(LogoutEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LogoutEventBuilder toBuilder() => new LogoutEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LogoutEvent;
  }

  @override
  int get hashCode {
    return 944313782;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper(r'LogoutEvent').toString();
  }
}

class LogoutEventBuilder implements Builder<LogoutEvent, LogoutEventBuilder> {
  _$LogoutEvent? _$v;

  LogoutEventBuilder();

  @override
  void replace(LogoutEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LogoutEvent;
  }

  @override
  void update(void Function(LogoutEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LogoutEvent build() => _build();

  _$LogoutEvent _build() {
    final _$result = _$v ?? new _$LogoutEvent._();
    replace(_$result);
    return _$result;
  }
}

class _$SetAppLanguage extends SetAppLanguage {
  @override
  final String language;

  factory _$SetAppLanguage([void Function(SetAppLanguageBuilder)? updates]) =>
      (new SetAppLanguageBuilder()..update(updates))._build();

  _$SetAppLanguage._({required this.language}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        language, r'SetAppLanguage', 'language');
  }

  @override
  SetAppLanguage rebuild(void Function(SetAppLanguageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SetAppLanguageBuilder toBuilder() =>
      new SetAppLanguageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SetAppLanguage && language == other.language;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, language.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SetAppLanguage')
          ..add('language', language))
        .toString();
  }
}

class SetAppLanguageBuilder
    implements Builder<SetAppLanguage, SetAppLanguageBuilder> {
  _$SetAppLanguage? _$v;

  String? _language;
  String? get language => _$this._language;
  set language(String? language) => _$this._language = language;

  SetAppLanguageBuilder();

  SetAppLanguageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _language = $v.language;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SetAppLanguage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SetAppLanguage;
  }

  @override
  void update(void Function(SetAppLanguageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SetAppLanguage build() => _build();

  _$SetAppLanguage _build() {
    final _$result = _$v ??
        new _$SetAppLanguage._(
            language: BuiltValueNullFieldError.checkNotNull(
                language, r'SetAppLanguage', 'language'));
    replace(_$result);
    return _$result;
  }
}

class _$AddTaskEvent extends AddTaskEvent {
  @override
  final String title;
  @override
  final String? subTitle;

  factory _$AddTaskEvent([void Function(AddTaskEventBuilder)? updates]) =>
      (new AddTaskEventBuilder()..update(updates))._build();

  _$AddTaskEvent._({required this.title, this.subTitle}) : super._() {
    BuiltValueNullFieldError.checkNotNull(title, r'AddTaskEvent', 'title');
  }

  @override
  AddTaskEvent rebuild(void Function(AddTaskEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddTaskEventBuilder toBuilder() => new AddTaskEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AddTaskEvent &&
        title == other.title &&
        subTitle == other.subTitle;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, title.hashCode);
    _$hash = $jc(_$hash, subTitle.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AddTaskEvent')
          ..add('title', title)
          ..add('subTitle', subTitle))
        .toString();
  }
}

class AddTaskEventBuilder
    implements Builder<AddTaskEvent, AddTaskEventBuilder> {
  _$AddTaskEvent? _$v;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _subTitle;
  String? get subTitle => _$this._subTitle;
  set subTitle(String? subTitle) => _$this._subTitle = subTitle;

  AddTaskEventBuilder();

  AddTaskEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _title = $v.title;
      _subTitle = $v.subTitle;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddTaskEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AddTaskEvent;
  }

  @override
  void update(void Function(AddTaskEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AddTaskEvent build() => _build();

  _$AddTaskEvent _build() {
    final _$result = _$v ??
        new _$AddTaskEvent._(
            title: BuiltValueNullFieldError.checkNotNull(
                title, r'AddTaskEvent', 'title'),
            subTitle: subTitle);
    replace(_$result);
    return _$result;
  }
}

class _$UpdateTaskEvent extends UpdateTaskEvent {
  @override
  final TaskModel task;

  factory _$UpdateTaskEvent([void Function(UpdateTaskEventBuilder)? updates]) =>
      (new UpdateTaskEventBuilder()..update(updates))._build();

  _$UpdateTaskEvent._({required this.task}) : super._() {
    BuiltValueNullFieldError.checkNotNull(task, r'UpdateTaskEvent', 'task');
  }

  @override
  UpdateTaskEvent rebuild(void Function(UpdateTaskEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UpdateTaskEventBuilder toBuilder() =>
      new UpdateTaskEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UpdateTaskEvent && task == other.task;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, task.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'UpdateTaskEvent')..add('task', task))
        .toString();
  }
}

class UpdateTaskEventBuilder
    implements Builder<UpdateTaskEvent, UpdateTaskEventBuilder> {
  _$UpdateTaskEvent? _$v;

  TaskModel? _task;
  TaskModel? get task => _$this._task;
  set task(TaskModel? task) => _$this._task = task;

  UpdateTaskEventBuilder();

  UpdateTaskEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _task = $v.task;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UpdateTaskEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UpdateTaskEvent;
  }

  @override
  void update(void Function(UpdateTaskEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  UpdateTaskEvent build() => _build();

  _$UpdateTaskEvent _build() {
    final _$result = _$v ??
        new _$UpdateTaskEvent._(
            task: BuiltValueNullFieldError.checkNotNull(
                task, r'UpdateTaskEvent', 'task'));
    replace(_$result);
    return _$result;
  }
}

class _$LoadTasksEvent extends LoadTasksEvent {
  factory _$LoadTasksEvent([void Function(LoadTasksEventBuilder)? updates]) =>
      (new LoadTasksEventBuilder()..update(updates))._build();

  _$LoadTasksEvent._() : super._();

  @override
  LoadTasksEvent rebuild(void Function(LoadTasksEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoadTasksEventBuilder toBuilder() =>
      new LoadTasksEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoadTasksEvent;
  }

  @override
  int get hashCode {
    return 664030940;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper(r'LoadTasksEvent').toString();
  }
}

class LoadTasksEventBuilder
    implements Builder<LoadTasksEvent, LoadTasksEventBuilder> {
  _$LoadTasksEvent? _$v;

  LoadTasksEventBuilder();

  @override
  void replace(LoadTasksEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoadTasksEvent;
  }

  @override
  void update(void Function(LoadTasksEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoadTasksEvent build() => _build();

  _$LoadTasksEvent _build() {
    final _$result = _$v ?? new _$LoadTasksEvent._();
    replace(_$result);
    return _$result;
  }
}

class _$DeleteTaskEvent extends DeleteTaskEvent {
  @override
  final String taskId;

  factory _$DeleteTaskEvent([void Function(DeleteTaskEventBuilder)? updates]) =>
      (new DeleteTaskEventBuilder()..update(updates))._build();

  _$DeleteTaskEvent._({required this.taskId}) : super._() {
    BuiltValueNullFieldError.checkNotNull(taskId, r'DeleteTaskEvent', 'taskId');
  }

  @override
  DeleteTaskEvent rebuild(void Function(DeleteTaskEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DeleteTaskEventBuilder toBuilder() =>
      new DeleteTaskEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DeleteTaskEvent && taskId == other.taskId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, taskId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'DeleteTaskEvent')
          ..add('taskId', taskId))
        .toString();
  }
}

class DeleteTaskEventBuilder
    implements Builder<DeleteTaskEvent, DeleteTaskEventBuilder> {
  _$DeleteTaskEvent? _$v;

  String? _taskId;
  String? get taskId => _$this._taskId;
  set taskId(String? taskId) => _$this._taskId = taskId;

  DeleteTaskEventBuilder();

  DeleteTaskEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _taskId = $v.taskId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DeleteTaskEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$DeleteTaskEvent;
  }

  @override
  void update(void Function(DeleteTaskEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  DeleteTaskEvent build() => _build();

  _$DeleteTaskEvent _build() {
    final _$result = _$v ??
        new _$DeleteTaskEvent._(
            taskId: BuiltValueNullFieldError.checkNotNull(
                taskId, r'DeleteTaskEvent', 'taskId'));
    replace(_$result);
    return _$result;
  }
}

class _$GetAppLanguage extends GetAppLanguage {
  factory _$GetAppLanguage([void Function(GetAppLanguageBuilder)? updates]) =>
      (new GetAppLanguageBuilder()..update(updates))._build();

  _$GetAppLanguage._() : super._();

  @override
  GetAppLanguage rebuild(void Function(GetAppLanguageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetAppLanguageBuilder toBuilder() =>
      new GetAppLanguageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetAppLanguage;
  }

  @override
  int get hashCode {
    return 42217207;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper(r'GetAppLanguage').toString();
  }
}

class GetAppLanguageBuilder
    implements Builder<GetAppLanguage, GetAppLanguageBuilder> {
  _$GetAppLanguage? _$v;

  GetAppLanguageBuilder();

  @override
  void replace(GetAppLanguage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetAppLanguage;
  }

  @override
  void update(void Function(GetAppLanguageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetAppLanguage build() => _build();

  _$GetAppLanguage _build() {
    final _$result = _$v ?? new _$GetAppLanguage._();
    replace(_$result);
    return _$result;
  }
}

class _$GetUsersEvent extends GetUsersEvent {
  @override
  final int page;

  factory _$GetUsersEvent([void Function(GetUsersEventBuilder)? updates]) =>
      (new GetUsersEventBuilder()..update(updates))._build();

  _$GetUsersEvent._({required this.page}) : super._() {
    BuiltValueNullFieldError.checkNotNull(page, r'GetUsersEvent', 'page');
  }

  @override
  GetUsersEvent rebuild(void Function(GetUsersEventBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetUsersEventBuilder toBuilder() => new GetUsersEventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetUsersEvent && page == other.page;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, page.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetUsersEvent')..add('page', page))
        .toString();
  }
}

class GetUsersEventBuilder
    implements Builder<GetUsersEvent, GetUsersEventBuilder> {
  _$GetUsersEvent? _$v;

  int? _page;
  int? get page => _$this._page;
  set page(int? page) => _$this._page = page;

  GetUsersEventBuilder();

  GetUsersEventBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _page = $v.page;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetUsersEvent other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetUsersEvent;
  }

  @override
  void update(void Function(GetUsersEventBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetUsersEvent build() => _build();

  _$GetUsersEvent _build() {
    final _$result = _$v ??
        new _$GetUsersEvent._(
            page: BuiltValueNullFieldError.checkNotNull(
                page, r'GetUsersEvent', 'page'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
