import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:maids_test/core/resources/string_manager.dart';
import 'package:maids_test/features/home/presentation/screens/done_task_screen.dart';
import 'package:maids_test/features/home/presentation/screens/home_screen.dart';
import 'package:maids_test/features/home/presentation/screens/new_task_screen.dart';
import 'package:stylish_bottom_bar/stylish_bottom_bar.dart';

class AdminMainScreen extends StatefulWidget {
  final int? screenIndex;

  const AdminMainScreen({
    super.key,
    required this.screenIndex,
  });

  @override
  State<AdminMainScreen> createState() => _AdminMainScreenState();
}

class _AdminMainScreenState extends State<AdminMainScreen> {
  dynamic selected;

  @override
  void initState() {
    selected = widget.screenIndex ?? 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      body: SafeArea(
        child: bottomNavScreen.elementAt(selected),
      ),
      bottomNavigationBar: StylishBottomBar(
        elevation: 10.0,
        onTap: (index) {
          setState(() {
            selected = index;
          });
        },
        currentIndex: selected,
        option: BubbleBarOptions(
          barStyle: BubbleBarStyle.horizontal,
          bubbleFillStyle: BubbleFillStyle.fill,
          inkEffect: true,
          opacity: 0.3,
          iconSize: 25.sp,
        ),
        items: [
          BottomBarItem(
            icon: const Icon(Icons.library_books_outlined),
            title: Text(StringManager.home.tr()),
          ),
          BottomBarItem(
            icon: const Icon(Icons.offline_bolt_outlined),
            title: Text(StringManager.newTasks.tr()),
            backgroundColor: Colors.blue,
          ),
          BottomBarItem(
            icon: const Icon(Icons.check_circle_outlined),
            title: Text(StringManager.doneTasks.tr()),
            backgroundColor: Colors.blue,
          ),
        ],
        hasNotch: true,
        borderRadius: BorderRadius.circular(20.r),
      ),
    );
  }

  List<Widget> bottomNavScreen = [
    const HomeScreen(),
    const NewTaskScreen(),
    const DoneTaskScreen(),
  ];
}
