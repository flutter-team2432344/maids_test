import 'package:flutter/material.dart';

class ColorManager {
  static const Color secondary = Color(0xFFFFA011);
  static const Color primary = Color(0xFF6011FC);
  static const Color white = Colors.white;
  static const Color black = Colors.black;
}
