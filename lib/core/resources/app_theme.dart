import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:maids_test/core/resources/theme_manager.dart';


ThemeData get appTheme => ThemeData.light().copyWith(
      scaffoldBackgroundColor: Colors.white,
      brightness: Brightness.light,
      textTheme: textTheme,
      appBarTheme: appBarTheme,
      colorScheme: colorScheme,
    );

AppBarTheme get appBarTheme => const AppBarTheme(
      surfaceTintColor: Colors.white,
      shadowColor: Colors.black,
      systemOverlayStyle: SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
        statusBarColor: ColorManager.white,
        systemNavigationBarDividerColor: ColorManager.white,
      ),
    );

ColorScheme get colorScheme => const ColorScheme.light(
      brightness: Brightness.light,
      background: ColorManager.white,
      inversePrimary: ColorManager.primary,
      primary: ColorManager.primary,
    );

TextTheme get textTheme => TextTheme(
      displayLarge: TextStyle(
        color: Colors.black,
        fontSize: 45.sp,
        fontWeight: FontWeight.bold,
      ),
      displayMedium: TextStyle(
        color: Colors.white,
        fontSize: 21.sp,
      ),
      displaySmall: TextStyle(
        color: const Color.fromARGB(255, 234, 234, 234),
        fontSize: 14.sp,
        fontWeight: FontWeight.w400,
      ),
      headlineMedium: TextStyle(
        color: Colors.grey,
        fontSize: 17.sp,
      ),
      headlineSmall: TextStyle(
        color: Colors.grey,
        fontSize: 16.sp,
      ),
      titleLarge: TextStyle(
        fontSize: 40.sp,
        color: Colors.black,
        fontWeight: FontWeight.w300,
      ),
      titleMedium: TextStyle(
        color: Colors.grey,
        fontSize: 16.sp,
        fontWeight: FontWeight.w300,
      ),
      titleSmall: const TextStyle(
        color: Colors.black,
        fontWeight: FontWeight.w500,
      ),
    );
