import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:maids_test/features/auth/presentation/screens/sign_in_screen.dart';
import 'package:maids_test/features/home/presentation/screens/add_task_screen.dart';
import 'package:maids_test/features/home/presentation/screens/done_task_screen.dart';
import 'package:maids_test/features/home/presentation/screens/home_screen.dart';
import 'package:maids_test/features/home/presentation/screens/new_task_screen.dart';
import 'package:maids_test/features/home/presentation/screens/users_screen.dart';
import 'package:maids_test/features/splash/presentation/screens/splash_screen.dart';

class GoRouterObserver extends NavigatorObserver {
  @override
  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {}

  @override
  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {}

  @override
  void didRemove(Route<dynamic> route, Route<dynamic>? previousRoute) {}

  @override
  void didReplace({Route<dynamic>? newRoute, Route<dynamic>? oldRoute}) {}
}

final _rootNavigatorKey = GlobalKey<NavigatorState>();
final _shellNavigatorKey = GlobalKey<NavigatorState>();

final GoRouter goRouter = GoRouter(
  initialLocation: SplashScreen.kPath,
  observers: [GoRouterObserver()],
  navigatorKey: _rootNavigatorKey,
  routes: [
    GoRoute(
      path: SplashScreen.kPath,
      pageBuilder: (context, state) => customTransitionPage(
        const SplashScreen(),
      ),
    ),
    GoRoute(
      path: SignInScreen.kPath,
      pageBuilder: (context, state) => customTransitionPage(
        const SignInScreen(),
      ),
    ),
    GoRoute(
      path: HomeScreen.kPath,
      pageBuilder: (context, state) => customTransitionPage(
        const HomeScreen(),
      ),
    ),
    GoRoute(
      path: UsersScreen.kPath,
      pageBuilder: (context, state) => customTransitionPage(
        const UsersScreen(),
      ),
    ),
    GoRoute(
      path: AddTaskScreen.kPath,
      pageBuilder: (context, state) => customTransitionPage(
        const AddTaskScreen(),
      ),
    ),
    GoRoute(
      path: DoneTaskScreen.kPath,
      pageBuilder: (context, state) => customTransitionPage(
        const DoneTaskScreen(),
      ),
    ),
    GoRoute(
      path: NewTaskScreen.kPath,
      pageBuilder: (context, state) => customTransitionPage(
        const NewTaskScreen(),
      ),
    )
  ],
);

CustomTransitionPage customTransitionPage(Widget child) {
  return CustomTransitionPage(
    transitionDuration: const Duration(seconds: 1),
    child: child,
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      return FadeTransition(
        opacity: CurveTween(curve: Curves.easeInOutCirc).animate(animation),
        child: child,
      );
    },
  );
}
