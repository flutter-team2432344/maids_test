class AssetsManager {
  static const baseImageAsset = "assets/images/";
  static const logo = "${baseImageAsset}logo.webp";
  static const emty = "${baseImageAsset}emty.json";
}
