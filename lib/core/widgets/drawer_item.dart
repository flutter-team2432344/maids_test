import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DrawerItems extends StatelessWidget {
  final String name;
  final String item1;
  final String item2;
  final IconData icon;
  final VoidCallback onTap1;
  final VoidCallback onTap2;
  final TextStyle? item1Style;
  final TextStyle? item2Style;
  const DrawerItems(
      {super.key,
      required this.name,
      required this.item1,
      required this.item2,
      required this.icon,
      required this.onTap1,
      required this.onTap2,
      this.item1Style,
      this.item2Style});

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text(name, style: TextStyle(fontSize: 16.sp, color: Colors.white)),
      leading: Icon(icon, size: 18.sp, color: Colors.white),
      collapsedIconColor: Colors.white,
      children: [
        ListTile(
          title: Text(item1,
              style: item1Style ??
                  TextStyle(fontSize: 18.sp, color: Colors.white)),
          onTap: onTap1,
        ),
        ListTile(
          title: Text(item2,
              style: item2Style ??
                  TextStyle(fontSize: 18.sp, color: Colors.white)),
          onTap: onTap2,
        ),
      ],
    );
  }
}
