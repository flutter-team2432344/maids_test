import 'dart:ui';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:go_router/go_router.dart';
import 'package:maids_test/core/resources/string_manager.dart';
import 'package:maids_test/core/resources/theme_manager.dart';
import 'package:maids_test/core/utils/constants.dart';
import 'package:maids_test/core/widgets/drawer_item.dart';
import 'package:maids_test/features/home/presentation/screens/add_task_screen.dart';
import 'package:maids_test/features/home/presentation/screens/done_task_screen.dart';
import 'package:maids_test/features/home/presentation/screens/new_task_screen.dart';
import 'package:maids_test/features/home/presentation/screens/users_screen.dart';
import 'package:maids_test/features/splash/presentation/screens/splash_screen.dart';
import 'package:maids_test/features/home/presentation/bloc/home_bloc.dart';
import 'package:maids_test/features/home/presentation/bloc/home_state.dart';
import 'package:maids_test/injecation_container.dart';

class BaseDrawer extends StatefulWidget {
  const BaseDrawer({super.key});

  @override
  State<BaseDrawer> createState() => _BaseDrawerState();
}

class _BaseDrawerState extends State<BaseDrawer> {
  final bloc = sl<HomeBloc>();
  final GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    bloc.getAppLanguage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Locale? currentLocal = EasyLocalization.of(context)!.currentLocale;
    return Drawer(
      width: 210.w,
      child: Stack(
        children: <Widget>[
          BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: 5.0,
              sigmaY: 5.0,
            ),
            child: Container(
              decoration: BoxDecoration(
                color: ColorManager.primary.withOpacity(0.5),
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x3F000000),
                    blurRadius: 10.20,
                    offset: Offset(0, 4),
                    spreadRadius: 0,
                  )
                ],
              ),
            ),
          ),
          Center(
            child: BlocBuilder<HomeBloc, HomeState>(
              bloc: bloc,
              builder: (context, state) {
                return ListView(
                  padding: EdgeInsets.symmetric(
                    vertical: 60.h,
                  ),
                  children: <Widget>[
                    Title(
                      color: Colors.white,
                      child: Text(
                        StringManager.menu.tr(),
                        style: TextStyle(
                          fontSize: 28.sp,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 10.h),
                    ListTile(
                      leading: const Icon(
                        Icons.offline_bolt_outlined,
                        color: Colors.white,
                      ),
                      title: Text(StringManager.newTasks.tr(),
                          style: const TextStyle(color: Colors.white)),
                      onTap: () {
                        context.push(NewTaskScreen.kPath);
                      },
                    ),
                    SizedBox(height: 10.h),
                    ListTile(
                      leading: const Icon(
                        Icons.check_circle_outlined,
                        color: Colors.white,
                      ),
                      title: Text(StringManager.doneTasks.tr(),
                          style: const TextStyle(color: Colors.white)),
                      onTap: () {
                        context.push(DoneTaskScreen.kPath);
                      },
                    ),
                    SizedBox(height: 10.h),
                    ListTile(
                      leading: const Icon(
                        FontAwesomeIcons.userGroup,
                        color: Colors.white,
                      ),
                      title: Text(StringManager.employees.tr(),
                          style: const TextStyle(color: Colors.white)),
                      onTap: () {
                        context.push(UsersScreen.kPath);
                      },
                    ),
                    SizedBox(height: 10.h),
                    DrawerItems(
                      name: StringManager.language.tr(),
                      item1: StringManager.english.tr(),
                      item1Style: TextStyle(
                        fontSize: 18.sp,
                        color: currentLocal == const Locale('en', 'US')
                            ? Colors.purple
                            : Colors.white,
                      ),
                      item2: StringManager.arabic.tr(),
                      item2Style: TextStyle(
                        fontSize: 18.sp,
                        color: currentLocal == const Locale('ar', 'AE')
                            ? Colors.purple
                            : Colors.white,
                      ),
                      icon: FontAwesomeIcons.language,
                      onTap1: () async {
                        setState(() {
                          if (currentLocal == const Locale('ar', 'AE')) {
                            context.setLocale(const Locale('en', 'US'));
                            bloc.setAppLanguage(AppLanguageKeys.en);
                            context.pushReplacement(SplashScreen.kPath);
                          } else {
                            Fluttertoast.showToast(
                              msg: 'Langugae already English',
                            );
                          }
                        });
                      },
                      onTap2: () async {
                        setState(() {
                          if (currentLocal == const Locale('en', 'US')) {
                            context.setLocale(const Locale('ar', 'AE'));
                            bloc.setAppLanguage(AppLanguageKeys.ar);
                            context.pushReplacement(SplashScreen.kPath);
                          } else {
                            Fluttertoast.showToast(
                              msg: 'Langugae already Arabic',
                            );
                          }
                        });
                      },
                    ),
                    SizedBox(height: 10.h),
                    ListTile(
                      leading: const Icon(
                        Icons.logout,
                        color: Colors.white,
                      ),
                      title: Text(StringManager.logOut.tr(),
                          style: const TextStyle(color: Colors.white)),
                      onTap: () {
                        bloc.logout();
                        context.pushReplacement(SplashScreen.kPath);
                      },
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
