import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class ErrorHandlingInterceptor extends Interceptor {
  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    debugPrint('Error occurred: $err');
    handler.next(err);
  }
}

class RequestLoggingInterceptor extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    debugPrint('Request: ${options.method} ${options.uri}');
    if (options.data != null) {
      debugPrint('Request body: ${options.data}');
    }
    handler.next(options);
  }
}

class ResponseLoggingInterceptor extends Interceptor {
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    debugPrint(
        'Response: ${response.statusCode} ${response.requestOptions.uri}');
    if (response.data != null) {
      debugPrint('Response body: ${response.data}');
    }
    handler.next(response);
  }
}
