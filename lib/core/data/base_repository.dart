import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import '../error/exceptions.dart';
import '../error/failures.dart';
import '../network/network_info.dart';
import 'base_local_data_source.dart';

typedef FutureEitherOr<T> = Future<Either<Failure, T>> Function();

typedef RequestBody<T> = Future<T> Function(String token, String language);

typedef LocalRequestBody<T> = Future<T> Function();

abstract class BaseRepository {
  Either<Failure, String> getToken();

  Future<Either<Failure, String>> getLanguage();

  Future<Either<Failure, T>> checkNetwork<T>(FutureEitherOr<T> body);

  Future<Either<Failure, T>> localRequest<T>(
    LocalRequestBody<T> body,
  );

  Future<Either<Failure, T>> requestWithTokenAndLanguage<T>(
    RequestBody<T> body,
  );
}

@LazySingleton(as: BaseRepository)
class BaseRepositoryImpl implements BaseRepository {
  final BaseLocalDataSource baseLocalDataSource;
  final NetworkInfo networkInfo;

  BaseRepositoryImpl({
    required this.baseLocalDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, T>> checkNetwork<T>(FutureEitherOr<T> body) async {
    if (await networkInfo.isConnected) {
      return body();
    } else {
      return const Left(ServerFailure(error: "No Internet Connection"));
    }
  }

  @override
  Future<Either<Failure, String>> getLanguage() async {
    final language = baseLocalDataSource.getAppLanguage();

    debugPrint('Language is $language\n');

    return language.isNotEmpty ? Right(language) : const Left(CacheFailure());
  }

  @override
  Either<Failure, String> getToken() {
    final token = baseLocalDataSource.token;

    debugPrint('Token is "$token"\n');

    return Right(token);
  }

  @override
  Future<Either<Failure, T>> requestWithTokenAndLanguage<T>(
    RequestBody<T> body,
  ) async {
    debugPrint('RequestWithTokenAndLanguage\n');

    final tokenOrFailure = getToken();

    return tokenOrFailure.fold(
      (failure) => const Left(CacheFailure(error: 'Cannot get token')),
      (token) async {
        final languageOrFailure = await getLanguage();

        return languageOrFailure.fold(
          (failure) => const Left(CacheFailure(error: 'Cannot get language')),
          (language) async {
            try {
              final T t = await body(token, language);
              return Right(t);
            } on HandledException catch (e) {
              return Left(ServerFailure(error: e.error));
            }
          },
        );
      },
    );
  }

  @override
  Future<Either<Failure, T>> localRequest<T>(LocalRequestBody<T> body) async {
    debugPrint('BaseRequest\n');

    try {
      final T t = await body();
      return Right(t);
    } on HandledException catch (e) {
      return Left(CacheFailure(error: e.error));
    }
  }
}
