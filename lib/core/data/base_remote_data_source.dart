import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:maids_test/core/data/base_local_data_source.dart';
import 'package:maids_test/core/data/error_handling_interceptor.dart';
import 'package:maids_test/core/models/base_list_response_model.dart';
import 'package:maids_test/core/models/base_response_model.dart';
import 'package:maids_test/core/utils/constants.dart';
import '../error/exceptions.dart';
import '../error/status_code_handler.dart';

abstract class BaseRemoteDataSource {
  @protected
  Future<T?> performGetRequest<T>({
    required String endpoint,
    Map<String, dynamic>? queryParameters,
  });

  @protected
  Future<List<T>> performGetListRequest<T>({
    required String endpoint,
    Map<String, dynamic>? queryParameters,
  });

  @protected
  Future<T?> performPostRequest<T>({
    required String endpoint,
    dynamic data,
    Map<String, dynamic>? queryParameters,
  });

  @protected
  Future<T?> performPutRequest<T>({
    required String endpoint,
    dynamic data,
    Map<String, dynamic>? queryParameters,
  });

  @protected
  Future<T?> performDeleteRequest<T>({
    required String endpoint,
    dynamic data,
    Map<String, dynamic>? queryParameters,
  });
}

@LazySingleton(as: BaseRemoteDataSource)
class BaseRemoteDataSourceImpl extends BaseRemoteDataSource {
  final Dio dio;
  final BaseLocalDataSource localDataSource;

  BaseRemoteDataSourceImpl({
    required this.dio,
    required this.localDataSource,
  }) {
    dio.interceptors.add(ErrorHandlingInterceptor());
    dio.interceptors.add(RequestLoggingInterceptor());
  }
  @override
  Future<T?> performPostRequest<T>({
    required String endpoint,
    String? language,
    dynamic data,
    Map<String, dynamic>? queryParameters,
  }) async {
    debugPrint("performPostRequest\n");
    String? token = localDataSource.token;

    try {
      debugPrint("Sending request...\n");
      debugPrint("token is $token \n");
      final response = await dio.post(
        endpoint,
        data: data,
        queryParameters: queryParameters,
        options: GetOptions.getOptionsWithToken(
          token,
          language: language ?? 'en',
        ),
      );

      debugPrint("Decoding response...\n");
      final result = BaseResponseModel.fromJson(
        json.decode(response.data),
        (json) => json,
      );

      debugPrint("Switching result\n");

      if (result.token != null || result.token != '') {
        debugPrint("Result is true\n");
        return result.data;
      } else {
        debugPrint("Result is false\n");
        throw ServerException(
            error: result.message ?? ErrorMessage.somethingWentWrong.tr());
      }
    } on DioException catch (e) {
      debugPrint("Dio Error :(\n");

      if (e.response != null) {
        debugPrint("Dio error and the response is not null\n");
        throw statusCodeHandler(e.response!);
      } else {
        debugPrint("Dio error and the response is null\n");
        throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
      }
    } catch (e) {
      if (e is HandledException) {
        rethrow;
      } else if (e is ParseException) {
        debugPrint(
          "\n*** Failed to parse the model please check the base or base list response model ***\n",
        );
        throw HandledException(error: ErrorMessage.somethingWentWrong.tr());
      } else {
        debugPrint("Request fail and the error not handled\n");
        throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
      }
    }
  }

  @override
  Future<T?> performPutRequest<T>({
    required String endpoint,
    dynamic data,
    Map<String, dynamic>? queryParameters,
  }) async {
    debugPrint("performPutRequest\n");
    String? token = localDataSource.token;

    try {
      debugPrint("Sending request...\n");
      final response = await dio.put(
        endpoint,
        data: data,
        queryParameters: queryParameters,
        options: GetOptions.getOptionsWithToken(
          token,
        ),
      );

      debugPrint("Decoding response...\n");
      final result = BaseResponseModel<T>.fromJson(
        json.decode(response.data),
        (json) => json,
      );

      debugPrint("Switching result\n");

      if (result.status ?? false) {
        debugPrint("Result is true\n");
        return result.data;
      } else {
        debugPrint("Result is false\n");
        throw ServerException(
          error: result.message ?? ErrorMessage.somethingWentWrong.tr(),
        );
      }
    } on DioException catch (e) {
      debugPrint("Dio Error :(\n");

      if (e.response != null) {
        debugPrint("Dio error and the response is not null\n");
        throw statusCodeHandler(e.response!);
      } else {
        debugPrint("Dio error and the response is null\n");
        throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
      }
    } catch (e) {
      if (e is HandledException) {
        rethrow;
      } else if (e is ParseException) {
        debugPrint(
          "\n*** Failed to parse the model please check the base or base list response model ***\n",
        );
        throw HandledException(error: ErrorMessage.somethingWentWrong.tr());
      } else {
        debugPrint("Request fail and the error not handled\n");
        throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
      }
    }
  }

  @override
  Future<T?> performDeleteRequest<T>({
    required String endpoint,
    dynamic data,
    Map<String, dynamic>? queryParameters,
  }) async {
    debugPrint("performDeleteRequest\n");
    String? token = localDataSource.token;

    try {
      debugPrint("Sending request...\n");
      final response = await dio.delete(
        endpoint,
        data: data,
        queryParameters: queryParameters,
        options: GetOptions.getOptionsWithToken(
          token,
        ),
      );

      debugPrint("Decoding response...\n");
      final result = BaseResponseModel<T>.fromJson(
        json.decode(response.data),
        (json) => json,
      );

      debugPrint("Switching result\n");

      if (result.status ?? false) {
        debugPrint("Result is true\n");
        return result.data;
      } else {
        debugPrint("Result is false\n");
        throw ServerException(
            error: result.message ?? ErrorMessage.somethingWentWrong.tr());
      }
    } on DioException catch (e) {
      debugPrint("Dio Error :(\n");

      if (e.response != null) {
        debugPrint("Dio error and the response is not null\n");
        throw statusCodeHandler(e.response!);
      } else {
        debugPrint("Dio error and the response is null\n");
        throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
      }
    } catch (e) {
      if (e is HandledException) {
        rethrow;
      } else if (e is ParseException) {
        debugPrint(
          "\n*** Failed to parse the model please check the base or base list response model ***\n",
        );
        throw HandledException(error: ErrorMessage.somethingWentWrong.tr());
      } else {
        debugPrint("Request fail and the error not handled\n");
        debugPrint('error is : ${e.toString()}');
        throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
      }
    }
  }

  @override
  Future<T> performGetRequest<T>({
    required String endpoint,
    Map<String, dynamic>? queryParameters,
  }) async {
    debugPrint("PerformGetRequest");
    String? token = localDataSource.token;

    try {
      debugPrint("Sending request...\n");
      late final Response response;
      response = await dio.get(
        endpoint,
        queryParameters: queryParameters,
        options: GetOptions.getOptionsWithToken(
          token,
        ),
      );

      debugPrint("Decoding response...\n");

      final result = BaseResponseModel<T>.fromJson(
        json.decode(response.data),
        (json) => json,
      );

      debugPrint("Switching result\n");

      if (result.status ?? false) {
        debugPrint("Result is true\n");
        return result.data!;
      } else {
        debugPrint("Result is false\n");
        throw ServerException(
          error: result.message ?? ErrorMessage.somethingWentWrong.tr(),
        );
      }
    } on DioException catch (e) {
      debugPrint("Dio Error :(\n");

      if (e.response != null) {
        debugPrint("Dio error and the response is not null\n");
        throw statusCodeHandler(e.response!);
      } else {
        debugPrint("Dio error and the response is null\n");
        throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
      }
    } catch (e) {
      if (e is HandledException) {
        rethrow;
      } else if (e is ParseException) {
        debugPrint(
          "\n*** Failed to parse the model please check the base or base list response model ***\n",
        );
        throw HandledException(error: ErrorMessage.somethingWentWrong.tr());
      } else {
        debugPrint("Request fail and the error not handled\n");
        debugPrint('error is  ${e.toString()}');
        throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
      }
    }
  }

  @override
  Future<List<T>> performGetListRequest<T>({
    required String endpoint,
    Map<String, dynamic>? queryParameters,
  }) async {
    debugPrint("PerformGetListRequest");
    String? token = localDataSource.token;

    try {
      debugPrint("Sending request...\n");
      late final Response response;
      response = await dio.get(
        endpoint,
        queryParameters: queryParameters,
        options: GetOptions.getOptionsWithToken(
          token,
        ),
      );

      debugPrint("Decoding response...\n");
      final result = BaseListResponseModel<T>.fromJson(
        json.decode(response.data),
        (json) => json,
      );

      debugPrint("Switching result\n");

      if (result.status ?? false) {
        debugPrint("Result is true\n");
        return result.data!;
      } else {
        debugPrint("Result is false\n");
        throw ServerException(
            error: result.message ?? ErrorMessage.somethingWentWrong.tr());
      }
    } on DioException catch (e) {
      debugPrint("Dio Error :(\n");

      if (e.response != null) {
        debugPrint("Dio error and the response is not null\n");
        throw statusCodeHandler(e.response!);
      } else {
        debugPrint("Dio error and the response is null\n");
        throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
      }
    } catch (e) {
      if (e is HandledException) {
        rethrow;
      } else if (e is ParseException) {
        debugPrint(
          "\n*** Failed to parse the model please check the base or base list response model ***\n",
        );
        throw HandledException(error: ErrorMessage.somethingWentWrong.tr());
      } else {
        debugPrint("Request fail and the error not handled\n");
        throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
      }
    }
  }

  void handleGenericError(dynamic error) {
    if (error is HandledException) {
      throw error;
    } else if (error is ParseException) {
      throw HandledException(error: ErrorMessage.somethingWentWrong.tr());
    } else {
      throw ServerException(error: ErrorMessage.somethingWentWrong.tr());
    }
  }
}
