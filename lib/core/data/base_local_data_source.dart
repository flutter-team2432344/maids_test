import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/constants.dart';

abstract class BaseLocalDataSource {
  String get token;

  String get language;

  String get phoneNumber;

  Future<void> logout();

  Future<void> setAppLanguage(String value);

  String getAppLanguage();
}

@LazySingleton(as: BaseLocalDataSource)
class BaseLocalDataSourceImp implements BaseLocalDataSource {
  final SharedPreferences sharedPreferences;

  BaseLocalDataSourceImp({
    required this.sharedPreferences,
  });

  @override
  String get language {
    return sharedPreferences.getString(LocalStorageKeys.appLanguage) ?? "en";
  }

  @override
  Future<void> setAppLanguage(String value) async {
    await sharedPreferences.setString(
      LocalStorageKeys.appLanguage,
      value,
    );
  }

  @override
  String get token {
    return sharedPreferences.getString(LocalStorageKeys.apiToken) ?? "";
  }

  @override
  Future<void> logout() async {
    await sharedPreferences.clear();
  }

  @override
  String get phoneNumber {
    return sharedPreferences.getString(LocalStorageKeys.phoneNumber) ?? "";
  }

  @override
  String getAppLanguage() =>
      sharedPreferences.getString(LocalStorageKeys.appLanguage) ?? 'en';
}
