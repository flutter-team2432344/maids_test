import 'package:colorize/colorize.dart';
import 'package:flutter/cupertino.dart';

class Dev {
  Dev._();

  static void console(List<dynamic> list) {
    debugPrint(
      Colorize("First").bgLightRed().white().bold().italic().initial +
          Colorize(" ==> ").bgLightRed().white().bold().italic().initial +
          Colorize("********************************")
              .apply(Styles.BLACK)
              .bgWhite()
              .bold()
              .italic()
              .initial,
    );

    for (var element in list) {
      if (element is List) {
        for (int index = 0; index < element.length; index++) {
          debugPrint(
            Colorize("ITEM ${index.toString()}")
                    .apply(Styles.RED)
                    .bold()
                    .italic()
                    .initial +
                Colorize(" ==> ").apply(Styles.WHITE).bold().italic().initial +
                Colorize(element[index].toString())
                    .apply(Styles.GREEN)
                    .bold()
                    .italic()
                    .initial,
          );
        }
      } else {
        debugPrint(
          Colorize("ELEMENT").apply(Styles.RED).bold().italic().initial +
              Colorize(" ==> ").apply(Styles.WHITE).bold().italic().initial +
              Colorize(element.toString())
                  .apply(Styles.GREEN)
                  .bold()
                  .italic()
                  .initial,
        );
      }
    }
    debugPrint(
      Colorize("END").bgLightRed().white().bold().italic().initial +
          Colorize(" ==> ").bgLightRed().white().bold().italic().initial +
          Colorize("**********************************")
              .apply(Styles.BLACK)
              .bgWhite()
              .bold()
              .italic()
              .initial,
    );
  }

  static void observerPrint(dynamic name, dynamic value, [dynamic additional]) {
    debugPrint(
      Colorize(name.toString())
              .apply(Styles.BLUE)
              .bgBlack()
              .bold()
              .italic()
              .initial +
          Colorize(" ==> ")
              .apply(Styles.BLUE)
              .bgBlack()
              .bold()
              .italic()
              .initial +
          Colorize(value.toString())
              .apply(Styles.YELLOW)
              .bgDarkGray()
              .bold()
              .italic()
              .initial,
    );
  }

  static void networkPrint(dynamic name, dynamic value) {
    debugPrint(
      Colorize(name.toString())
              .apply(Styles.BLACK)
              .bgWhite()
              .bold()
              .italic()
              .initial +
          Colorize(" ==> ")
              .apply(Styles.BLACK)
              .bgWhite()
              .bold()
              .italic()
              .initial +
          Colorize(value.toString())
              .apply(Styles.YELLOW)
              .bold()
              .italic()
              .initial,
    );
  }
}
