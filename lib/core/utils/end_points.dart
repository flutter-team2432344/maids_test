class EndPoints {
  static const String baseUrl = 'https://reqres.in/api/';
  static const String signIn = 'login';
  static String getUsers(int page) => 'users?page=$page';
}
