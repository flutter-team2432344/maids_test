import 'package:dio/dio.dart';

class AppLanguageKeys {
  static const String en = 'en';
  static const String ar = 'ar';
}

class ErrorMessage {
  static const String somethingWentWrong = "somethingWentWrong";
  static String error400 = "error400";
  static String error401 = "error401";
  static String error403 = "error403";
  static String error404 = "error404";
  static String error412 = "error412";
  static String error500 = "error500";
  static String error503 = "error503";
}

class LocalStorageKeys {
  static const phoneNumber = "phoneNumner";
  static const userId = 'user_id';
  static const apiToken = 'api_token';
  static const appLanguage = "app_language";
}

class GetOptions {
  static Options options = Options();

  static Options getOptionsWithToken(String? token,
      {String language = "en", int isPagination = 0}) {
    if (token != null && token.isNotEmpty) {
      options.headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
        'Accept-Language': language,
        'is-pagination': isPagination,
      };
    } else {
      options.headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Accept-Language': language,
      };
    }
    return options;
  }
}
