import 'package:dio/dio.dart';

class RequestBody {
  static FormData signIn({
    required String email,
    required String password,
  }) {
    return FormData.fromMap({
      'email': email,
      'password': password,
    });
  }
}
