import 'dart:core';

import 'package:hive/hive.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:maids_test/features/home/data/model/task.dart';

import 'package:path_provider/path_provider.dart';

import 'hive_boxes.dart';
import 'hive_constants.dart';

abstract class HiveInitializer {
  static Future<void> initialize() async {
    var documentsDirectory = await getApplicationDocumentsDirectory();

    Hive.init(documentsDirectory.path);
    bool isNotRegistered(int typeId) {
      return !Hive.isAdapterRegistered(typeId);
    }

    if (isNotRegistered(HiveConstants.todoItem)) {
      Hive.registerAdapter<TaskModel>(TaskModelAdapter());
    }
  }

  static Future<Box<TaskModel>> initBox() {
    return Hive.openBox<TaskModel>(HiveBoxes.todoBox);
  }
}
