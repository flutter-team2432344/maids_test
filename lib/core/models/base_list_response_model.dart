class BaseListResponseModel<T> {
  final bool? status;
  final String? message;
  final List<T>? data;

  BaseListResponseModel({this.status, this.message, this.data});

  factory BaseListResponseModel.fromJson(Map<String, dynamic> json, T Function(dynamic) fromJsonT) {
    return BaseListResponseModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      data: (json['data'] as List<dynamic>?)?.map((e) => fromJsonT(e)).toList(),
    );
  }

  Map<String, dynamic> toJson(Object? Function(T) toJsonT) {
    return {
      'status': status,
      'message': message,
      'data': data?.map((e) => toJsonT(e)).toList(),
    };
  }
}
