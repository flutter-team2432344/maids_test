class BaseResponseModel<T> {
  final bool? status;
  final String? message;
  final T? data;
  final String? token;
  BaseResponseModel({
    this.status,
    this.message,
    this.data,
    this.token,
  });

  factory BaseResponseModel.fromJson(
      Map<String, dynamic> json, T Function(dynamic) fromJsonT) {
    return BaseResponseModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      data: json.containsKey('data') ? fromJsonT(json['data']) : null,
      token: json['token'] as String?,
    );
  }
}
