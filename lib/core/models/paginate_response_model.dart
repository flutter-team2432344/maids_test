// ignore_for_file: overridden_fields

import 'package:maids_test/core/entities/paginate_list.dart';
import 'package:equatable/equatable.dart';

class PaginatedResponse<T extends Equatable> extends PaginateList {
  final int currentPage;
  final int lastPage;
  @override
  final List<T> data;

  const PaginatedResponse({
    required this.currentPage,
    required this.lastPage,
    required this.data,
  }) : super(
          data: data,
          count: lastPage,
          page: currentPage,
        );

  factory PaginatedResponse.fromJson(
      Map<String, dynamic> json, T Function(Map<String, dynamic>) fromJson) {
    return PaginatedResponse(
      currentPage: json['page'],
      lastPage: json['total_pages'],
      data: (json['data'] as List)
          .map((item) => fromJson(item as Map<String, dynamic>))
          .toList(),
    );
  }
}
