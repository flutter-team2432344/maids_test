import 'package:injectable/injectable.dart';
import 'package:maids_test/app/data/data_source/local_data_source/app_local_data_source.dart';
import 'package:maids_test/app/domain/app_repository.dart';

import '../../../core/data/base_local_data_source.dart';
import '../../../core/data/base_repository.dart';
import '../../../core/network/network_info.dart';

@LazySingleton(as: AppRepository)
class AppRepositoryImp extends BaseRepositoryImpl implements AppRepository {
  final AppLocalDataSource local;

  AppRepositoryImp(
    this.local, {
    required BaseLocalDataSource baseLocalDataSource,
    required NetworkInfo networkInfo,
  }) : super(
          baseLocalDataSource: baseLocalDataSource,
          networkInfo: networkInfo,
        );

  @override
  String getAppLanguage() {
    return baseLocalDataSource.getAppLanguage();
  }

  @override
  Future<void> setAppLanguage(String value) async {
    await baseLocalDataSource.setAppLanguage(value);
  }

  @override
  Future<void> forceLogout() async {
    await baseLocalDataSource.logout();
  }
}