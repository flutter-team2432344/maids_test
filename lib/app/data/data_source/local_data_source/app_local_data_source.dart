import '../../../../core/data/base_local_data_source.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class AppLocalDataSource extends BaseLocalDataSource {}

@LazySingleton(as: AppLocalDataSource)
class AppLocalDataSourceImp extends BaseLocalDataSourceImp
    implements AppLocalDataSource {
  AppLocalDataSourceImp({
    required SharedPreferences sharedPreferences,
  }) : super(
          sharedPreferences: sharedPreferences,
        );
}
