import '../../../core/data/base_repository.dart';

abstract class AppRepository extends BaseRepository {
  String getAppLanguage();

  Future<void> setAppLanguage(String value);

  Future<void> forceLogout();
}