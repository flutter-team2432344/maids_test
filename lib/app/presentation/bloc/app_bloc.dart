
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:maids_test/app/domain/app_repository.dart';
import 'app_event.dart';
import 'app_state.dart';

@injectable
class AppBloc extends Bloc<AppEvent, AppState> {
  final AppRepository repository;

  void getAppLanguage() {
    add(GetAppLanguage());
  }

  void setAppLanguage(String languageCode) {
    add(SetAppLanguage(
      (b) => b..languageCode = languageCode,
    ));
  }

  @factoryMethod
  AppBloc(this.repository) : super(AppState.initial()) {
    on<AppEvent>((event, emit) async {
      if (event is GetAppLanguage) {
        final language = repository.getAppLanguage();
        emit(state.rebuild((b) => b..appLanguage = language));
      }
      if (event is SetAppLanguage) {
        await repository.setAppLanguage(event.languageCode);
        emit(
          state.rebuild(
            (b) => b..appLanguage = event.languageCode,
          ),
        );
      }
    });
  }
}
