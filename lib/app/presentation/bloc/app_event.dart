library app_event;

import 'package:built_value/built_value.dart';

part 'app_event.g.dart';

abstract class AppEvent {}

abstract class GetColor extends AppEvent
    implements Built<GetColor, GetColorBuilder> {
  //getter fields
  GetColor._();

  factory GetColor([Function(GetColorBuilder b) updates]) = _$GetColor;

  factory GetColor.initial() {
    return GetColor((b) => b);
  }
}

abstract class GetAppLanguage extends AppEvent
    implements Built<GetAppLanguage, GetAppLanguageBuilder> {

  GetAppLanguage._();

  factory GetAppLanguage([Function(GetAppLanguageBuilder b) updates]) =
      _$GetAppLanguage;
}

abstract class SetAppLanguage extends AppEvent
    implements Built<SetAppLanguage, SetAppLanguageBuilder> {
  String get languageCode;

  SetAppLanguage._();

  factory SetAppLanguage([void Function(SetAppLanguageBuilder b) updates]) =
      _$SetAppLanguage;
}