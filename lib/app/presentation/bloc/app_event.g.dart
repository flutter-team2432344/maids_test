// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_event.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetColor extends GetColor {
  factory _$GetColor([void Function(GetColorBuilder)? updates]) =>
      (new GetColorBuilder()..update(updates))._build();

  _$GetColor._() : super._();

  @override
  GetColor rebuild(void Function(GetColorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetColorBuilder toBuilder() => new GetColorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetColor;
  }

  @override
  int get hashCode {
    return 895779090;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper(r'GetColor').toString();
  }
}

class GetColorBuilder implements Builder<GetColor, GetColorBuilder> {
  _$GetColor? _$v;

  GetColorBuilder();

  @override
  void replace(GetColor other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetColor;
  }

  @override
  void update(void Function(GetColorBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetColor build() => _build();

  _$GetColor _build() {
    final _$result = _$v ?? new _$GetColor._();
    replace(_$result);
    return _$result;
  }
}

class _$GetAppLanguage extends GetAppLanguage {
  factory _$GetAppLanguage([void Function(GetAppLanguageBuilder)? updates]) =>
      (new GetAppLanguageBuilder()..update(updates))._build();

  _$GetAppLanguage._() : super._();

  @override
  GetAppLanguage rebuild(void Function(GetAppLanguageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetAppLanguageBuilder toBuilder() =>
      new GetAppLanguageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetAppLanguage;
  }

  @override
  int get hashCode {
    return 42217207;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper(r'GetAppLanguage').toString();
  }
}

class GetAppLanguageBuilder
    implements Builder<GetAppLanguage, GetAppLanguageBuilder> {
  _$GetAppLanguage? _$v;

  GetAppLanguageBuilder();

  @override
  void replace(GetAppLanguage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetAppLanguage;
  }

  @override
  void update(void Function(GetAppLanguageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetAppLanguage build() => _build();

  _$GetAppLanguage _build() {
    final _$result = _$v ?? new _$GetAppLanguage._();
    replace(_$result);
    return _$result;
  }
}

class _$SetAppLanguage extends SetAppLanguage {
  @override
  final String languageCode;

  factory _$SetAppLanguage([void Function(SetAppLanguageBuilder)? updates]) =>
      (new SetAppLanguageBuilder()..update(updates))._build();

  _$SetAppLanguage._({required this.languageCode}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        languageCode, r'SetAppLanguage', 'languageCode');
  }

  @override
  SetAppLanguage rebuild(void Function(SetAppLanguageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SetAppLanguageBuilder toBuilder() =>
      new SetAppLanguageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SetAppLanguage && languageCode == other.languageCode;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, languageCode.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SetAppLanguage')
          ..add('languageCode', languageCode))
        .toString();
  }
}

class SetAppLanguageBuilder
    implements Builder<SetAppLanguage, SetAppLanguageBuilder> {
  _$SetAppLanguage? _$v;

  String? _languageCode;
  String? get languageCode => _$this._languageCode;
  set languageCode(String? languageCode) => _$this._languageCode = languageCode;

  SetAppLanguageBuilder();

  SetAppLanguageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _languageCode = $v.languageCode;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SetAppLanguage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SetAppLanguage;
  }

  @override
  void update(void Function(SetAppLanguageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SetAppLanguage build() => _build();

  _$SetAppLanguage _build() {
    final _$result = _$v ??
        new _$SetAppLanguage._(
            languageCode: BuiltValueNullFieldError.checkNotNull(
                languageCode, r'SetAppLanguage', 'languageCode'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
