import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:maids_test/app/presentation/screens/app.dart';
import 'package:maids_test/core/local_data_source/hive_initializer.dart';
import 'package:maids_test/injecation_container.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();  
  await HiveInitializer.initialize();
  await configureDependencies();
  runApp(
    EasyLocalization(
      path: 'assets/language',
      supportedLocales: const [Locale('en', 'US'), Locale('ar', 'AE')],
      child: const App(),
    ),
  );
}
